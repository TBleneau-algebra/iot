#include <WiFi.h>
#include <SD.h>
#include <BLEScan.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>
#include <BLEDevice.h>
#include <BLEBeacon.h>
#include "configuration.h"
#include "esp32.h"

static FloorConfig floorConfig;
static SSIDConfig ssidConfig;
static ESP32Config esp32Config;
static ServerConfig serverConfig;
static std::vector<struct BeaconConfig> beacons;

BLEScan* esp32BLEScan;

void setup()
{ 
    Serial.begin(115200);

    Serial.println("ESP32 connecting WiFi...");
    connectToWiFi();

    Serial.println("ESP32 creating BLE Scan...");
    createBLEScan();

    Serial.println("ESP32 retrieving the card interface...");
    getESP32Interface();

    Serial.println("ESP32 sending the card interface to the API...");
    if (postESP32() >= 400) {
      Serial.println("The API has returned an error. Exiting...");
      exit(1);
    }
}

void loop() {
    if (WiFi.status() == WL_CONNECTED) {
      scanBLEDevice();
      postBeaconData();
    } else {
      Serial.println("Error - Failed to connect to the WiFi. Exiting...");
      exit(1);
  }
  beacons.clear();
  esp32BLEScan->clearResults();
  delay(10000);
}


/***********
  WIFI PART
************/

void connectToWiFi() {
  WiFi.mode(WIFI_MODE_STA);

  if (ssidConfig.ssid_password.c_str() != "" && ssidConfig.ssid_password.c_str() != NULL)
    WiFi.begin(ssidConfig.ssid.c_str(), ssidConfig.ssid_password.c_str());
  else
    WiFi.begin(ssidConfig.ssid.c_str());
  
  while (WiFi.status() != WL_CONNECTED) {
    Serial.println("Connecting to WiFi..");
    delay(500);
  }
  Serial.println("ESP32 connected to WiFi");
}


/****************
  BLUETOOTH PART
****************/

void createBLEScan() {
  BLEDevice::init("ESP32 BLE Device"); 
  Serial.println("ESP32 BLE initialised");

  Serial.println("ESP32 activating scan...");
  esp32BLEScan = BLEDevice::getScan();
  esp32BLEScan->setActiveScan(true);
  esp32BLEScan->setInterval(100);
  esp32BLEScan->setWindow(99);
}

void scanBLEDevice() {
  struct BeaconConfig config;
  
  Serial.println("ESP32 scanning BLE devices...");
  BLEScanResults foundDevices = esp32BLEScan->start(5, false);
  Serial.println("ESP32 scan completed");

  Serial.println("ESP32 finding Beacon devices...");
  for (int idx = 0; idx < foundDevices.getCount(); idx++) {
    if (in_array(foundDevices.getDevice(idx).getAddress().toString(), beacons_address)) {
      config = getResult(foundDevices.getDevice(idx));
      beacons.push_back(config);
    }
  }
  Serial.println("ESP32 found " + String(beacons.size()) + " beacon devices...");
}

/************************
  BEACON INTERFACE  PART
*************************/

struct BeaconConfig getResult(BLEAdvertisedDevice advertisedDevice) {
  uint8_t beaconServiceData[100];
  uint8_t beaconManufacturerData[100];
  
  struct BeaconConfig config;
  
  BLEBeacon bleBeacon = BLEBeacon();
  
  std::string manufacturerData = advertisedDevice.getManufacturerData();
  std::string serviceData = advertisedDevice.getServiceData();     
  
  serviceData.copy((char *)beaconServiceData, serviceData.length(), 0);
  manufacturerData.copy((char *)beaconManufacturerData, manufacturerData.length(), 0);
  
  bleBeacon.setData(manufacturerData);

  /*************************
     BLEBeacon information
  *************************/

  config.minor = bleBeacon.getMinor();
  config.major = bleBeacon.getMajor();
  config.txPower = bleBeacon.getSignalPower();
  config.manufacturerId = bleBeacon.getManufacturerId();
  config.serviceUUID = bleBeacon.getProximityUUID().toString().c_str();

  /************************************
     BLEAdvertisedDevice information
  ************************************/
  
  config.name = advertisedDevice.getName().c_str();
  config.appearance = advertisedDevice.getAppearance();
  config.macAddress = advertisedDevice.getAddress().toString().c_str();
  config.addressType = addressTypeToString(advertisedDevice.getAddressType()).c_str();
  config.serviceData = advertisedDevice.getServiceData().c_str();
  
  if (advertisedDevice.getServiceDataUUID().toString() == "<NULL>")
    config.serviceDataUUID = "";
  else
    config.serviceDataUUID = advertisedDevice.getServiceDataUUID().toString().c_str();

  config.rssi = advertisedDevice.getRSSI();
  config.payload = *advertisedDevice.getPayload();
  config.payloadLength = advertisedDevice.getPayloadLength();

  /*************************
     Distance calculation
  *************************/

  config.txPower = -67;
  config.distance =  pow(10, ((float)config.txPower - (float)config.rssi) / (10 * ENVIRONMENTAL_FACTOR));

  return config;
}
    

/***********************
  ESP32 INTERFACE  PART
************************/

void getESP32Interface() {
  esp32Config.ssid = WiFi.SSID();
  esp32Config.bssid = getBSSIDMacAddress();
  esp32Config.ipAddress = WiFi.localIP().toString();
  esp32Config.macAddress = WiFi.macAddress();
  esp32Config.rssi = WiFi.RSSI();
}

String getBSSIDMacAddress() {
  String digit;
  String mac_address;
  uint8_t* bssid = WiFi.BSSID();

  for (int i = 0; i < 6; ++i) {
    digit = String(*bssid, HEX);
    if (digit.length() < 2)
      mac_address += String("0") + digit;
    else
      mac_address += digit; 
    if (i < 5)
      mac_address += ':';
    bssid++;
  }
  return mac_address;
}


/*****************
  CONTROLLER PART
*****************/

int postESP32() {
  HTTPClient http;
  int httpResponseCode;

  http.begin(serverConfig.server_name + ESP32_URL_REGISTER);
  
  http.addHeader("Content-Type", "application/json");

  http.addHeader("esp32-floor", floorConfig.floor);
  http.addHeader("esp32-position", floorConfig.position);

  Serial.println("ESP32 posting arduino card information...");
  httpResponseCode = http.POST(ESP32ConfigToJSON());

  if (httpResponseCode != 201) {
    http.end();
    
    Serial.println("ESP32 already registered");
    return updateESP32();      
  }
  
  http.end();

  Serial.println("ESP32 information posted");
  return httpResponseCode;
}

int updateESP32() {
  HTTPClient http;
  int httpResponseCode;

  http.begin(serverConfig.server_name + ESP32_URL_UPDATE);
  
  http.addHeader("Content-Type", "application/json");

  http.addHeader("esp32-floor", floorConfig.floor);
  http.addHeader("esp32-position", floorConfig.position);

  Serial.println("ESP32 updating arduino card information...");
  httpResponseCode = http.PUT(ESP32ConfigToJSON());

  if (httpResponseCode == 200) {
    Serial.println("ESP32 information updated");
  }
  
  http.end();
  return httpResponseCode;
}

int postBeaconData() {
  HTTPClient http;
  String beaconData;
  int httpResponseCode;

  Serial.println("ESP32 posting beacon(s) data...");  
  
  for (size_t idx = 0; idx < beacons.size(); idx++) {
    http.begin(serverConfig.server_name + BEACON_URL_REGISTER);
    
    http.addHeader("Content-Type", "application/json");
    
    http.addHeader("esp32-identity", esp32Config.macAddress);
    http.addHeader("esp32-floor", floorConfig.floor);
    http.addHeader("esp32-position", floorConfig.position);
    
    http.addHeader("beacon-rssi", String(beacons[idx].rssi));
    http.addHeader("beacon-txPower", String(beacons[idx].txPower));
    http.addHeader("beacon-payload", String(beacons[idx].payload));
    http.addHeader("beacon-payloadLength", String(beacons[idx].payloadLength));
    http.addHeader("beacon-distance", String(beacons[idx].distance));
  
    beaconData = BeaconConfigToJSON(beacons[idx]);
    
    httpResponseCode = http.POST(beaconData);
    
    if (httpResponseCode != 201) {
      http.end();  
      
      Serial.println("ESP32 updating beacon(s) data...");
      httpResponseCode = updateBeaconData(beacons[idx]);
    }
    
    http.end();
  }
  
  http.end();
  return httpResponseCode;
}

int updateBeaconData(struct BeaconConfig &beacon) {
  HTTPClient http;
  int httpResponseCode;

  http.begin(serverConfig.server_name + BEACON_URL_UPDATE);
  
  http.addHeader("Content-Type", "application/json");
  
  http.addHeader("esp32-identity", esp32Config.macAddress);
  http.addHeader("esp32-floor", floorConfig.floor);
  http.addHeader("esp32-position", floorConfig.position);

  http.addHeader("beacon-rssi", String(beacon.rssi));
  http.addHeader("beacon-txPower", String(beacon.txPower));
  http.addHeader("beacon-payload", String(beacon.payload));
  http.addHeader("beacon-payloadLength", String(beacon.payloadLength));
  http.addHeader("beacon-distance", String(beacon.distance));
    
  httpResponseCode = http.PUT(BeaconConfigToJSON(beacon));

  http.end();
  return httpResponseCode;
}


/*************
  UTILS PART
**************/

String ESP32ConfigToJSON() {
  String result;
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();

  root["rssi"] = esp32Config.rssi;
  root["ssid"] = esp32Config.ssid;
  root["bssid"] = esp32Config.bssid;
  root["ipAddress"] = esp32Config.ipAddress;
  root["macAddress"] = esp32Config.macAddress;

  root.printTo(result);

  Serial.print("ESP32 Configuration serialized : ");
  Serial.println(result);
  return result;
}

String BeaconConfigToJSON(struct BeaconConfig &config) {
  String result;
  StaticJsonBuffer<1200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();

  root["name"] = config.name;
  root["macAddress"] = config.macAddress;
  root["addressType"] = config.addressType;
  root["manufacturerId"] = config.manufacturerId;
  root["serviceUUID"] = config.serviceUUID;
  root["serviceData"] = config.serviceData;
  root["serviceDataUUID"] = config.serviceDataUUID;
  root["appearance"] = config.appearance;
  root["minor"] = config.minor;
  root["major"] = config.major;

  root.printTo(result);

  Serial.print("Beacon serialized : ");
  Serial.println(result);

  Serial.print("Beacon with MAC address " + config.macAddress + " - distance, rssi and txPower found : ");
  Serial.println("distance : " + String(config.distance) + " , RSSI : " + String(config.rssi) + ", txPower : " + String(config.txPower));
  return result;
}

bool in_array(const std::string &value, const std::vector<std::string> &array){
  return std::find(array.begin(), array.end(), value) != array.end();
}

const std::string addressTypeToString(esp_ble_addr_type_t type) {
  switch(type) {
    case BLE_ADDR_TYPE_PUBLIC:
      return "BLE_ADDR_TYPE_PUBLIC";
    case BLE_ADDR_TYPE_RANDOM:
      return "BLE_ADDR_TYPE_RANDOM";
    case BLE_ADDR_TYPE_RPA_PUBLIC:
      return "BLE_ADDR_TYPE_RPA_PUBLIC";
    case BLE_ADDR_TYPE_RPA_RANDOM:
      return "BLE_ADDR_TYPE_RPA_RANDOM";
    default:
      return "BLE_ADDR_TYPE_UNKNOWN";
  }
} 
