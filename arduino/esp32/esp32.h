struct ESP32Config {
  long rssi;
  String ssid;
  String bssid;
  String ipAddress;
  String macAddress;
};

struct BeaconConfig {
  String name;
  String macAddress;
  String addressType;
  
  String serviceUUID;
  String serviceData;
  String serviceDataUUID;
  
  uint16_t minor;  
  uint16_t major;
  uint16_t manufacturerId;
  
  int rssi;
  int8_t txPower;
  uint16_t appearance;
  uint8_t payload;
  size_t payloadLength;

  float distance;
};
