# SQL Database

### Initialize the database in your local machine

On your local machine, you must first connect to the mysql daemon

```sh
      mysql -u root -p
```

You need to know the password of your root user. Enter the password of your root user and you are finally connected as a super user to MySQL
Now, you just have to create a user and your database for the connection.

```sh 
      CREATE DATABASE locating_equipment_database;

      CREATE USER 'locatingEquipmentUser'@'localhost' IDENTIFIED BY 'password';

      GRANT ALL PRIVILEGES ON locating_equipment_database.* TO 'locatingEquipmentUser'@'localhost';

      FLUSH PRIVILEGES;

      QUIT
```

### Initialize the database in your docker container

With docker, your must first access to your docker container. In this project, the container name is 'locating-equipment-db'.

```sh
      docker exec -it locating-equipment-db /bin/bash
```

Once access to the docker container has been established, you have to connect to the mysql daemon :

```sh
      mysql -u root -p
```

You need to know the password of your root user. Enter the password of your root user and you are finally connected as a super user to MySQL
Now, you just have to create a user and your database for the connection.

```sh
      CREATE DATABASE locating_equipment_database;

      CREATE USER 'locatingEquipmentUser'@'%' IDENTIFIED BY 'password';

      GRANT ALL PRIVILEGES ON locating_equipment_database.* TO 'locatingEquipmentUser'@'%';

      FLUSH PRIVILEGES;

      QUIT
```

If you use a MySQL docker container on your local machine, you should create a sample iptables rule to open linux iptables firewall.

```sh
      iptables -A INPUT -i eth0 -p tcp --destination-port 3306 -j ACCEPT
```

In the Server, we only want to allow remote connection from the web API. 
For that, you should create a sample iptables rule which only allows access to the MySQL docker container to an ip address (that of the web API)

```sh
      iptables -A INPUT -i eth0 -s <ip_address> -p tcp --destination-port 3306 -j ACCEPT
```

For security reasons, Docker configures the iptables rules to prevent containers from forwarding traffic from outside the host machine, on Linux hosts. 
Docker sets the default policy of the FORWARD chain to DROP.

If the container running on host1 needs the ability to communicate directly with a container on host2, you need a route from host1 to host2.
Setting the policy to ACCEPT accomplishes this.

```sh
      sudo iptables -P FORWARD ACCEPT
```

