package hr.algebra.locatingequipement.serverapp.api.v1.controller;

import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.FloorEntity;
import hr.algebra.locatingequipement.serverapp.api.v1.entity.response.FloorResponseEntity;
import hr.algebra.locatingequipement.serverapp.api.v1.repository.FloorRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
@Tag(name = "Floor's Controller", description = "The routes described below provide access to the FloorEntity entities")
public class FloorController {

    private final FloorRepository floorRepository;

    public FloorController(FloorRepository floorRepository) {
        this.floorRepository = floorRepository;
    }

    @Operation(summary = "Find all floors", description = "Retrieve all floors available")
    @ApiResponse(responseCode = "200", description = "Successful request")
    @RequestMapping(method = GET, path = "/floor", produces = "application/json")
    public ResponseEntity<FloorResponseEntity> getAll() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(new FloorResponseEntity(this.floorRepository.findAll(), HttpStatus.OK));
    }

    @Operation(summary = "Find floor by id", description = "Retrieve a floor with the id requested")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request"),
            @ApiResponse(responseCode = "404", description = "Notfound. Floor not found", content = {})
    })
    @RequestMapping(method = GET, path = "/floor/{floorId}", produces = "application/json")
    public ResponseEntity<FloorEntity> getById(@PathVariable Long floorId) {
        Optional<FloorEntity> floorEntity = this.floorRepository.findById(floorId);

        if (floorEntity.orElse(null) == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        return ResponseEntity.status(HttpStatus.OK).body(floorEntity.get());
    }
}
