package hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "esp32s_beacons")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)
public class ESP32BeaconEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "esp32_id")
    private ESP32Entity esp32;

    @ManyToOne
    @JoinColumn(name = "beacon_id")
    private BeaconEntity beacon;

    @JsonIgnore
    @OneToMany(mappedBy = "esp32Beacon", cascade = CascadeType.ALL)
    private Set<ESP32BeaconHistoryEntity> history = new HashSet<>();

    @NotNull
    @Column(name = "beacon_rssi")
    private int rssi;

    @NotNull
    @Column(name = "beacon_tx_power")
    private int txPower;

    @Column(name = "beacon_payload")
    private int payload;

    @Column(name = "beacon_payload_length")
    private int payloadLength;

    @Column(name = "beacon_distance")
    private double distance;

    @Column(name = "created_at", nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Column(name = "updated_at", nullable = false)
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    public ESP32BeaconEntity() {
    }

    public ESP32BeaconEntity(ESP32Entity esp32Entity, BeaconEntity beaconEntity) {
        this.esp32 = esp32Entity;
        this.beacon = beaconEntity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BeaconEntity getBeacon() {
        return beacon;
    }

    public void setBeacon(BeaconEntity beacon) {
        this.beacon = beacon;
    }

    public ESP32Entity getEsp32() {
        return esp32;
    }

    public void setEsp32(ESP32Entity esp32) {
        this.esp32 = esp32;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public int getTxPower() {
        return txPower;
    }

    public void setTxPower(int txPower) {
        this.txPower = txPower;
    }

    public int getPayload() {
        return payload;
    }

    public void setPayload(int payload) {
        this.payload = payload;
    }

    public int getPayloadLength() {
        return payloadLength;
    }

    public void setPayloadLength(int payloadLength) {
        this.payloadLength = payloadLength;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Set<ESP32BeaconHistoryEntity> getHistory() {
        return history;
    }

    public void setHistory(Set<ESP32BeaconHistoryEntity> history) {
        this.history = history;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
