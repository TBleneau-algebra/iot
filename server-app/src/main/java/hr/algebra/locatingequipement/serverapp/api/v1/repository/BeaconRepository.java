package hr.algebra.locatingequipement.serverapp.api.v1.repository;

import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.BeaconEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface BeaconRepository extends JpaRepository<BeaconEntity, Long> {

    BeaconEntity findByMacAddress(String macAddress);

    @Query(value = "SELECT DISTINCT beacon FROM BeaconEntity beacon " +
            "INNER JOIN beacon.esp32s esp32s " +
            "INNER JOIN esp32s.esp32 esp32 " +
            "WHERE esp32.floor.id = ?1 " +
            "AND esp32s.updatedAt > ?2")
    List<BeaconEntity> fetchBeaconEntityByFloorAndEsp32sAndUpdatedAt(Long floorId, LocalDateTime date);
}
