package hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.ESP32BeaconEntity;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

public class ESP32BeaconResponseEntity {
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<String> errors;

    private int code;

    private String message;

    @Nullable
    private ESP32BeaconEntity data;

    public ESP32BeaconResponseEntity(HttpStatus status) {
        this.message = status.getReasonPhrase();
        this.code = status.value();
        this.errors = new ArrayList<>();
    }

    public ESP32BeaconResponseEntity(ESP32BeaconEntity data, HttpStatus status) {
        this.data = data;
        this.message = status.getReasonPhrase();
        this.code = status.value();
        this.errors = new ArrayList<>();
    }

    public void fromBindingErrors(Errors errors) {
        for (ObjectError objectError : errors.getAllErrors()) {
            this.errors.add(objectError.getDefaultMessage());
        }
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ESP32BeaconEntity getData() {
        return data;
    }

    public void setData(ESP32BeaconEntity data) {
        this.data = data;
    }

}
