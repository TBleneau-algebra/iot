package hr.algebra.locatingequipement.serverapp.api.v1.documentation;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenAPIConfiguration {

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .components(new Components())
                .info(new Info()
                        .title("Locating Equipment Application API")
                        .version("1.0.0")
                        .description("This is the API documentation for the Locating Equipment project. " +
                                "This documentation lists all the available API routes with which the different " +
                                "actors of the project interact, as well as the Java entities stored in the database."));
    }
}
