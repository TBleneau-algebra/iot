package hr.algebra.locatingequipement.serverapp.api.v1.repository;

import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.ESP32BeaconHistoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ESP32BeaconHistoryRepository extends JpaRepository<ESP32BeaconHistoryEntity, Long> {

    @Query(nativeQuery = true, value = "SELECT AVG(beacon_distance) FROM esp32s_beacons_history " +
            "WHERE id NOT IN ?1 " +
            "AND esp32_beacon_id = ?2 " +
            "AND created_at >= ?3")
    Double fetchDistanceAverage(List<Long> esp32BeaconHistoryId, Long esp32BeaconId, LocalDateTime date);

    @Query(nativeQuery = true, value = "SELECT AVG(beacon_distance) FROM esp32s_beacons_history " +
            "WHERE esp32_beacon_id = ?1 " +
            "AND created_at >= ?2")
    Double fetchDistanceAverage(Long esp32BeaconId, LocalDateTime date);

    @Query(nativeQuery = true, value = "WITH orderedList AS (\n" +
            "    SELECT id, esp32_beacon_id, beacon_rssi, beacon_distance, ROW_NUMBER() OVER (ORDER BY beacon_rssi desc) AS row_n\n" +
            "    FROM esp32s_beacons_history\n" +
            "    WHERE esp32_beacon_id = ?1\n" +
            "     AND created_at >= ?2)," +
            "     iqrange (id, beacon_rssi, esp32_beacon_id, beacon_distance, q_three, q_one, outlier_range) AS (\n" +
            "         SELECT id,\n" +
            "                beacon_rssi,\n" +
            "                esp32_beacon_id,\n" +
            "                beacon_distance,\n" +
            "                (\n" +
            "                    SELECT beacon_rssi AS quartile_break\n" +
            "                    FROM orderedList\n" +
            "                    WHERE row_n = FLOOR((SELECT COUNT(*)\n" +
            "                                         FROM orderedList) * 0.75)\n" +
            "                )     AS q_three,\n" +
            "                (\n" +
            "                    SELECT beacon_rssi AS quartile_break\n" +
            "                    FROM orderedList\n" +
            "                    WHERE row_n = FLOOR((SELECT COUNT(*)\n" +
            "                                         FROM orderedList) * 0.25)\n" +
            "                )     AS q_one,\n" +
            "                1.05 * ((\n" +
            "                            SELECT beacon_rssi AS quartile_break\n" +
            "                            FROM orderedList\n" +
            "                            WHERE row_n = FLOOR((SELECT COUNT(*)\n" +
            "                                                 FROM orderedList) * 0.75)\n" +
            "                        ) - (\n" +
            "                            SELECT beacon_rssi AS quartile_break\n" +
            "                            FROM orderedList\n" +
            "                            WHERE row_n = FLOOR((SELECT COUNT(*)\n" +
            "                                                 FROM orderedList) * 0.25)\n" +
            "                        )\n" +
            "                    ) AS outlier_range\n" +
            "         FROM orderedList\n" +
            "     ),\n" +
            "     bounds as (\n" +
            "         SELECT id, beacon_rssi, MAX(q_three) + MAX(outlier_range) AS UPPER, MAX(q_one) - MAX(outlier_range) AS LOWER\n" +
            "         FROM iqrange\n" +
            "         GROUP BY id, beacon_rssi\n" +
            "     )\n" +
            "select id\n" +
            "FROM bounds\n" +
            "WHERE beacon_rssi >= LOWER \n" +
            "   OR beacon_rssi <= UPPER")
    List<Long> fetchOutlierValues(Long esp32BeaconId, LocalDateTime date);
}
