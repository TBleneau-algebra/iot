package hr.algebra.locatingequipement.serverapp.api.v1.controller;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.InputStream;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
@Tag(name = "Application's Controller", description = "The route described below provides access to the application " +
        "APK file")
public class ApplicationController {

    private final ServletContext servletContext;
    private static final String DIRECTORY = "/apk/";
    private static final String DEFAULT_FILE_NAME = "locating-equipment-app.apk";

    public ApplicationController(ServletContext servletContext) {
        this.servletContext = servletContext;
    }


    @ApiResponse(responseCode = "200", description = "Successful request")
    @RequestMapping(method = GET, path = "/application")
    public ResponseEntity<byte[]> downloadApp() throws IOException {
        MediaType mediaType;

        try {
            mediaType = MediaType.parseMediaType(this.servletContext.getMimeType(DEFAULT_FILE_NAME));
        } catch (RuntimeException e) {
            mediaType = MediaType.APPLICATION_OCTET_STREAM;
        }

        InputStream stream = ApplicationController.class.getResourceAsStream(DIRECTORY + DEFAULT_FILE_NAME);

        if (stream != null) {
            byte[] targetArray = IOUtils.toByteArray(stream);

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + DEFAULT_FILE_NAME)
                    .contentType(mediaType)
                    .contentLength(targetArray.length)
                    .body(targetArray);
        }
        return ResponseEntity.ok().body(null);
    }
}
