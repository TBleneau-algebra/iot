package hr.algebra.locatingequipement.serverapp.api.v1.repository;

import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.ESP32Entity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ESP32Repository extends JpaRepository<ESP32Entity, Long> {
    ESP32Entity findByMacAddress(String macAddress);
}
