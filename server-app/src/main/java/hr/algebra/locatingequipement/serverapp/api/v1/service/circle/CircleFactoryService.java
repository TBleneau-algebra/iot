package hr.algebra.locatingequipement.serverapp.api.v1.service.circle;

import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.ESP32BeaconEntity;
import hr.algebra.locatingequipement.serverapp.api.v1.entity.circle.CircleEntity;
import hr.algebra.locatingequipement.serverapp.api.v1.service.triangulation.TriangulationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class CircleFactoryService {

    private final TriangulationService triangulationService;

    public CircleFactoryService(TriangulationService triangulationService) {
        this.triangulationService = triangulationService;
    }

    public double getCircleXaxis(ESP32BeaconEntity esp32BeaconEntity) {
        String position = esp32BeaconEntity.getEsp32().getPosition().getName();

        if (StringUtils.equalsAny(position, "CORNER_BOTTOM_LEFT", "CORNER_TOP_LEFT")) {
            return 0;
        } else if (StringUtils.equalsAny(position, "CORNER_BOTTOM_RIGHT", "CORNER_TOP_RIGHT")) {
            return esp32BeaconEntity.getEsp32().getFloor().getxMax();
        }
        return 0;
    }

    public double getCircleYaxis(ESP32BeaconEntity esp32BeaconEntity) {
        String position = esp32BeaconEntity.getEsp32().getPosition().getName();

        if (StringUtils.equalsAny(position, "CORNER_TOP_LEFT", "CORNER_TOP_RIGHT")) {
            return esp32BeaconEntity.getEsp32().getFloor().getyMax();
        } else if (StringUtils.equalsAny(position, "CORNER_BOTTOM_RIGHT", "CORNER_BOTTOM_LEFT")) {
            return 0;
        }
        return 0;
    }

    public List<CircleEntity> getCircles(Map<ESP32BeaconEntity, Double> map) {
        List<CircleEntity> circleEntities = new ArrayList<>();

        for (Map.Entry<ESP32BeaconEntity, Double> entry : map.entrySet()) {
            double radius = entry.getValue();
            double xAxis = getCircleXaxis(entry.getKey());
            double yAxis = getCircleYaxis(entry.getKey());

            CircleEntity circleEntity = new CircleEntity(xAxis, yAxis, radius);
            circleEntities.add(circleEntity);
        }
        return circleEntities;
    }

    public CircleEntity getCircleTriangulated(List<CircleEntity> circleEntities) {
        return this.triangulationService.closestIntersection(circleEntities);
    }
}
