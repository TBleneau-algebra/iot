package hr.algebra.locatingequipement.serverapp.api.v1.controller;

import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.*;
import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.response.ESP32BeaconResponseEntity;
import hr.algebra.locatingequipement.serverapp.api.v1.entity.circle.CircleEntity;
import hr.algebra.locatingequipement.serverapp.api.v1.entity.response.BeaconListResponseEntity;
import hr.algebra.locatingequipement.serverapp.api.v1.entity.response.CircleResponseEntity;
import hr.algebra.locatingequipement.serverapp.api.v1.repository.*;
import hr.algebra.locatingequipement.serverapp.api.v1.service.circle.CircleFactoryService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
@Tag(name = "ESP32Beacon's Controller", description = "The routes described below provide access to ESP32BeaconEntity " +
        "   entities that represent a relationship between a BeaconEntity and an ESP32Entity entity")
public class ESP32BeaconController {

    private final ESP32Repository esp32Repository;
    private final BeaconRepository beaconRepository;
    private final FloorRepository floorRepository;
    private final ESP32BeaconRepository esp32BeaconRepository;
    private final ESP32AuthorizedRepository esp32AuthorizedRepository;
    private final ESP32BeaconHistoryRepository esp32BeaconHistoryRepository;

    private final CircleFactoryService circleFactoryService;

    public ESP32BeaconController(ESP32Repository esp32Repository,
                                 BeaconRepository beaconRepository,
                                 FloorRepository floorRepository,
                                 ESP32BeaconRepository esp32BeaconRepository,
                                 ESP32AuthorizedRepository esp32AuthorizedRepository,
                                 ESP32BeaconHistoryRepository esp32BeaconHistoryRepository,
                                 CircleFactoryService circleFactoryService) {
        this.esp32Repository = esp32Repository;
        this.beaconRepository = beaconRepository;
        this.floorRepository = floorRepository;
        this.esp32BeaconRepository = esp32BeaconRepository;
        this.esp32AuthorizedRepository = esp32AuthorizedRepository;
        this.esp32BeaconHistoryRepository = esp32BeaconHistoryRepository;
        this.circleFactoryService = circleFactoryService;
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request"),
            @ApiResponse(responseCode = "400", description = "Bad request. The floor ID passed as parameter does not exist"),
    })
    @RequestMapping(method = GET, path = "/beacon", produces = "application/json")
    public ResponseEntity<BeaconListResponseEntity> getAllByFloor(@RequestParam Long floorId) {
        BeaconListResponseEntity beaconListResponseEntity;

        Optional<FloorEntity> floorEntity = this.floorRepository.findById(floorId);
        if (!floorEntity.isPresent() || floorId == null) {
            beaconListResponseEntity = new BeaconListResponseEntity(HttpStatus.BAD_REQUEST);
            beaconListResponseEntity.getErrors().add("The floor does not exist");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(beaconListResponseEntity);
        }
        LocalDateTime date = LocalDateTime.now().minusMinutes(1);

        beaconListResponseEntity = new BeaconListResponseEntity(this.beaconRepository.fetchBeaconEntityByFloorAndEsp32sAndUpdatedAt(floorEntity.get().getId(), date),
                HttpStatus.OK);
        return ResponseEntity.status(HttpStatus.OK).body(beaconListResponseEntity);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successfully created"),
            @ApiResponse(responseCode = "400", description = "Bad request. Missing headers properties | " +
                    "The ESP32's position doesn't exist or doesn't match | " +
                    "This floor associated to the ESP32 doesn't exist or doesn't match | " +
                    "The ESP32 associated doesn't exist"),
            @ApiResponse(responseCode = "401", description = "The ESP32 associated isn't authorized"),
            @ApiResponse(responseCode = "409", description = "Conflict. Beacon already registered")
    })
    @RequestMapping(method = POST, path = "/beacon/register", produces = "application/json")
    public ResponseEntity<ESP32BeaconResponseEntity> createBeacon(
            @Parameter(description = "Http headers", required = true)
            @RequestHeader HttpHeaders httpHeaders,
            @Parameter(description = "ESP32 to register. Cannot be null or empty", required = true)
            @Valid @RequestBody BeaconEntity beaconEntity,
            Errors errors) {
        ESP32BeaconResponseEntity esp32BeaconResponseEntity = catchErrors(httpHeaders, errors);

        if (esp32BeaconResponseEntity != null) {
            return ResponseEntity.status(esp32BeaconResponseEntity.getCode()).body(esp32BeaconResponseEntity);
        }

        ESP32Entity esp32Entity = esp32Repository.findByMacAddress(httpHeaders.getFirst("esp32-identity"));

        BeaconEntity beaconExist = beaconRepository.findByMacAddress(beaconEntity.getMacAddress());

        if (beaconExist != null) {
            if (esp32BeaconRepository.findByEsp32AndBeacon(esp32Entity, beaconExist) != null) {
                esp32BeaconResponseEntity = new ESP32BeaconResponseEntity(HttpStatus.CONFLICT);
                esp32BeaconResponseEntity.getErrors().add("The relationship between this beacon and this ESP32 already exists");
                return ResponseEntity.status(HttpStatus.CONFLICT).body(esp32BeaconResponseEntity);
            }
        } else {
            beaconRepository.save(beaconEntity);
            beaconExist = beaconRepository.findByMacAddress(beaconEntity.getMacAddress());
        }

        ESP32BeaconEntity esp32BeaconEntity = new ESP32BeaconEntity(esp32Entity, beaconExist);

        this.setESP32BeaconProperties(esp32BeaconEntity, httpHeaders);

        esp32BeaconRepository.save(esp32BeaconEntity);

        esp32BeaconResponseEntity = new ESP32BeaconResponseEntity(HttpStatus.CREATED);
        esp32BeaconResponseEntity.setData(esp32BeaconEntity);
        return ResponseEntity.status(HttpStatus.CREATED).body(esp32BeaconResponseEntity);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully updated"),
            @ApiResponse(responseCode = "400", description = "Bad request. Missing headers properties | " +
                    "The ESP32's position doesn't exist or doesn't match | " +
                    "This floor associated to the ESP32 doesn't exist or doesn't match | " +
                    "The ESP32 associated doesn't exist | " +
                    "The relationship between this beacon and this ESP32 does not exist"),
            @ApiResponse(responseCode = "401", description = "The ESP32 associated isn't authorized"),
    })
    @RequestMapping(method = PUT, path = "/beacon/update", produces = "application/json")
    public ResponseEntity<ESP32BeaconResponseEntity> updateBeacon(
            @Parameter(description = "Http headers", required = true)
            @RequestHeader HttpHeaders httpHeaders,
            @Parameter(description = "Beacon to update. Cannot be null or empty", required = true)
            @Valid @RequestBody BeaconEntity beaconEntity,
            Errors errors) {
        ESP32BeaconResponseEntity esp32BeaconResponseEntity = catchErrors(httpHeaders, errors);

        if (esp32BeaconResponseEntity != null) {
            return ResponseEntity.status(esp32BeaconResponseEntity.getCode()).body(esp32BeaconResponseEntity);
        }

        ESP32Entity esp32Entity = esp32Repository.findByMacAddress(httpHeaders.getFirst("esp32-identity"));
        BeaconEntity beaconExist = beaconRepository.findByMacAddress(beaconEntity.getMacAddress());

        if (beaconExist == null) {
            esp32BeaconResponseEntity = new ESP32BeaconResponseEntity(HttpStatus.BAD_REQUEST);
            esp32BeaconResponseEntity.getErrors().add("The Beacon with MAC address " + beaconEntity.getMacAddress() + " does not exist");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(esp32BeaconResponseEntity);
        }

        ESP32BeaconEntity esp32BeaconEntity = esp32BeaconRepository.findByEsp32AndBeacon(esp32Entity, beaconExist);

        if (esp32BeaconEntity == null) {
            esp32BeaconResponseEntity = new ESP32BeaconResponseEntity(HttpStatus.BAD_REQUEST);
            esp32BeaconResponseEntity.getErrors().add("The relationship between this beacon and this ESP32 does not exist");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(esp32BeaconResponseEntity);
        }

        this.setESP32BeaconProperties(esp32BeaconEntity, httpHeaders);

        esp32BeaconEntity = esp32BeaconRepository.save(esp32BeaconEntity);

        ESP32BeaconHistoryEntity esp32BeaconHistoryEntity = new ESP32BeaconHistoryEntity(esp32BeaconEntity);

        esp32BeaconHistoryRepository.save(esp32BeaconHistoryEntity);

        esp32BeaconResponseEntity = new ESP32BeaconResponseEntity(HttpStatus.OK);
        esp32BeaconResponseEntity.setData(esp32BeaconEntity);
        return ResponseEntity.status(HttpStatus.OK).body(esp32BeaconResponseEntity);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully updated"),
            @ApiResponse(responseCode = "400", description = "Bad request. The beacon is not detected by any or less than three ESP32 | " +
                    "The position of this beacon has not been updated for more or less a minute"),
            @ApiResponse(responseCode = "404", description = "Not found. The beacon or the floor doesn't exist"),
    })
    @RequestMapping(method = GET, path = "/beacon/position", produces = "application/json")
    public ResponseEntity<CircleResponseEntity> positionBeacon(
            @Parameter(description = "Floor ID", required = true)
            @RequestParam Long floorId,
            @Parameter(description = "Beacon ID", required = true)
            @RequestParam Long beaconId) {

        CircleResponseEntity circleResponseEntity;

        Optional<BeaconEntity> beaconEntity = this.beaconRepository.findById(beaconId);
        if (!beaconEntity.isPresent()) {
            circleResponseEntity = new CircleResponseEntity(HttpStatus.NOT_FOUND);
            circleResponseEntity.getErrors().add("This object does not exist");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(circleResponseEntity);
        }

        Optional<FloorEntity> floorEntity = this.floorRepository.findById(floorId);
        if (!floorEntity.isPresent()) {
            circleResponseEntity = new CircleResponseEntity(HttpStatus.NOT_FOUND);
            circleResponseEntity.getErrors().add("The floor does not exist");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(circleResponseEntity);
        }

        List<ESP32BeaconEntity> esp32BeaconEntities = this.esp32BeaconRepository.findAllByEsp32FloorAndBeacon(floorEntity.get(), beaconEntity.get());
        if (esp32BeaconEntities.isEmpty() || esp32BeaconEntities.size() < 3) {
            circleResponseEntity = new CircleResponseEntity(HttpStatus.BAD_REQUEST);
            circleResponseEntity.getErrors().add("This object is not detected by any or less than three ESP32");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(circleResponseEntity);
        }

        LocalDateTime date = LocalDateTime.now().minusMinutes(2);
        Map<ESP32BeaconEntity, Double> map = new HashMap<>();

        for (ESP32BeaconEntity esp32BeaconEntity : esp32BeaconEntities) {
            Double average;
            List<Long> outliersIDs = this.esp32BeaconHistoryRepository.fetchOutlierValues(esp32BeaconEntity.getId(), date);

            if (!outliersIDs.isEmpty()) {
                average = this.esp32BeaconHistoryRepository.fetchDistanceAverage(outliersIDs, esp32BeaconEntity.getId(), date);
                if (average == null) {
                    average = this.esp32BeaconHistoryRepository.fetchDistanceAverage(esp32BeaconEntity.getId(), date);
                }

            } else {
                average = this.esp32BeaconHistoryRepository.fetchDistanceAverage(esp32BeaconEntity.getId(), date);
            }

            if (average == null) {
                circleResponseEntity = new CircleResponseEntity(HttpStatus.BAD_REQUEST);
                circleResponseEntity.getErrors().add("The position of this object has not been updated for more or less a minute");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(circleResponseEntity);
            }
            map.put(esp32BeaconEntity, average);
        }

        if (map.isEmpty()) {
            circleResponseEntity = new CircleResponseEntity(HttpStatus.BAD_REQUEST);
            circleResponseEntity.getErrors().add("The position of this object has not been updated for more or less a minute");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(circleResponseEntity);
        }

        List<CircleEntity> circleEntities = this.circleFactoryService.getCircles(map);
        circleResponseEntity = new CircleResponseEntity(this.circleFactoryService.getCircleTriangulated(circleEntities), HttpStatus.OK);
        return ResponseEntity.status(HttpStatus.OK).body(circleResponseEntity);
    }


    private void setESP32BeaconProperties(ESP32BeaconEntity esp32BeaconEntity, HttpHeaders httpHeaders) {
        esp32BeaconEntity.setRssi(Integer.parseInt(Objects.requireNonNull(httpHeaders.getFirst("beacon-rssi"))));
        esp32BeaconEntity.setTxPower(Integer.parseInt(Objects.requireNonNull(httpHeaders.getFirst("beacon-txPower"))));
        esp32BeaconEntity.setPayload(Integer.parseInt(Objects.requireNonNull(httpHeaders.getFirst("beacon-payload"))));
        esp32BeaconEntity.setPayloadLength(Integer.parseInt(Objects.requireNonNull(httpHeaders.getFirst("beacon-payloadLength"))));
        esp32BeaconEntity.setDistance(Float.parseFloat(Objects.requireNonNull(httpHeaders.getFirst("beacon-distance"))));
    }

    private ESP32BeaconResponseEntity catchErrors(HttpHeaders httpHeaders, Errors errors) {
        ESP32BeaconResponseEntity esp32BeaconResponseEntity;

        if (httpHeaders.getFirst("esp32-identity") == null ||
                httpHeaders.getFirst("esp32-floor") == null ||
                httpHeaders.getFirst("esp32-position") == null ||
                httpHeaders.getFirst("beacon-rssi") == null ||
                httpHeaders.getFirst("beacon-txPower") == null ||
                httpHeaders.getFirst("beacon-payload") == null ||
                httpHeaders.getFirst("beacon-payloadLength") == null ||
                httpHeaders.getFirst("beacon-distance") == null) {
            esp32BeaconResponseEntity = new ESP32BeaconResponseEntity(HttpStatus.BAD_REQUEST);
            esp32BeaconResponseEntity.getErrors().add("Missing headers properties.");
            return esp32BeaconResponseEntity;
        }

        if (errors.hasErrors()) {
            esp32BeaconResponseEntity = new ESP32BeaconResponseEntity(HttpStatus.BAD_REQUEST);
            esp32BeaconResponseEntity.fromBindingErrors(errors);
            return esp32BeaconResponseEntity;
        }

        if (esp32AuthorizedRepository.findByMacAddress(httpHeaders.getFirst("esp32-identity")) == null) {
            esp32BeaconResponseEntity = new ESP32BeaconResponseEntity(HttpStatus.UNAUTHORIZED);
            esp32BeaconResponseEntity.getErrors().add("This ESP32 isn't authorized");
            return esp32BeaconResponseEntity;
        }

        ESP32Entity esp32Entity = esp32Repository.findByMacAddress(httpHeaders.getFirst("esp32-identity"));

        if (esp32Entity == null) {
            esp32BeaconResponseEntity = new ESP32BeaconResponseEntity(HttpStatus.BAD_REQUEST);
            esp32BeaconResponseEntity.getErrors().add("The ESP32 card with MAC address " + httpHeaders.getFirst("esp32-identity") + " does not exist");
            return esp32BeaconResponseEntity;
        }

        if (!esp32Entity.getFloor().getName().equals(httpHeaders.getFirst("esp32-floor"))) {
            esp32BeaconResponseEntity = new ESP32BeaconResponseEntity(HttpStatus.BAD_REQUEST);
            esp32BeaconResponseEntity.getErrors().add("Bad Request. This floor doesn't exist or doesn't match");
            return esp32BeaconResponseEntity;
        }

        if (!esp32Entity.getPosition().getName().equals(httpHeaders.getFirst("esp32-position"))) {
            esp32BeaconResponseEntity = new ESP32BeaconResponseEntity(HttpStatus.BAD_REQUEST);
            esp32BeaconResponseEntity.getErrors().add("Bad Request. This position doesn't exist or doesn't match");
            return esp32BeaconResponseEntity;
        }

        return null;
    }

}
