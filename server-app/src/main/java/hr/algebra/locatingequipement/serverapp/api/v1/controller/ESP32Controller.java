package hr.algebra.locatingequipement.serverapp.api.v1.controller;

import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.ESP32AuthorizedEntity;
import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.ESP32Entity;
import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.FloorEntity;
import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.PositionEntity;
import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.response.ESP32ResponseEntity;
import hr.algebra.locatingequipement.serverapp.api.v1.repository.ESP32AuthorizedRepository;
import hr.algebra.locatingequipement.serverapp.api.v1.repository.ESP32Repository;
import hr.algebra.locatingequipement.serverapp.api.v1.repository.FloorRepository;
import hr.algebra.locatingequipement.serverapp.api.v1.repository.PositionRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
@Tag(name = "ESP32's Controller", description = "The routes described below provide access to the ESP32Entity entities")
public class ESP32Controller {

    private final ESP32Repository esp32Repository;
    private final ESP32AuthorizedRepository esp32AuthorizedRepository;

    private final FloorRepository floorRepository;
    private final PositionRepository positionRepository;

    public ESP32Controller(ESP32Repository esp32Repository, ESP32AuthorizedRepository esp32AuthorizedRepository,
                           FloorRepository floorRepository, PositionRepository positionRepository) {
        this.esp32Repository = esp32Repository;
        this.esp32AuthorizedRepository = esp32AuthorizedRepository;
        this.floorRepository = floorRepository;
        this.positionRepository = positionRepository;
    }

    @Operation(summary = "Register ESP32's information", description = "Register the information of an esp32 if its " +
            "MAC address is one of the authorized MAC addresses")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successfully created"),
            @ApiResponse(responseCode = "400", description = "Bad request. Floor or ESP32's position doesn't exist"),
            @ApiResponse(responseCode = "409", description = "Conflict. ESP32 already registered")
    })
    @RequestMapping(method = POST, path = "/esp32/register", produces = "application/json")
    public ResponseEntity<ESP32ResponseEntity> createESP32(
            @Parameter(description = "Http headers", required = true)
            @RequestHeader HttpHeaders httpHeaders,
            @Parameter(description = "ESP32 to register. Cannot be null or empty", required = true)
            @Valid @RequestBody ESP32Entity esp32Entity, Errors errors) {

        ESP32ResponseEntity esp32ReponseEntity = catchErrors(httpHeaders, esp32Entity.getMacAddress(), errors);

        if (esp32ReponseEntity != null) {
            return ResponseEntity.status(esp32ReponseEntity.getCode()).body(esp32ReponseEntity);
        }

        ESP32Entity esp32Exist = esp32Repository.findByMacAddress(esp32Entity.getMacAddress());
        if (esp32Exist != null) {
            esp32ReponseEntity = new ESP32ResponseEntity(HttpStatus.CONFLICT);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(esp32ReponseEntity);
        }

        FloorEntity floorEntity = this.floorRepository.findByName(httpHeaders.getFirst("esp32-floor"));
        if (floorEntity == null) {
            esp32ReponseEntity = new ESP32ResponseEntity(HttpStatus.BAD_REQUEST);
            esp32ReponseEntity.getErrors().add("Bad Request. Floor doesn't exist");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(esp32ReponseEntity);
        }

        PositionEntity positionEntity = this.positionRepository.findByName(httpHeaders.getFirst("esp32-position"));
        if (positionEntity == null) {
            esp32ReponseEntity = new ESP32ResponseEntity(HttpStatus.BAD_REQUEST);
            esp32ReponseEntity.getErrors().add("Bad Request. Position doesn't exist");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(esp32ReponseEntity);
        }

        esp32Entity.setFloor(floorEntity);
        esp32Entity.setPosition(positionEntity);

        esp32Repository.save(esp32Entity);

        esp32ReponseEntity = new ESP32ResponseEntity(esp32Entity, HttpStatus.CREATED);
        return ResponseEntity.status(HttpStatus.CREATED).body(esp32ReponseEntity);
    }

    @Operation(summary = "Update ESP32's information", description = "Updates the information of an esp32 if it is " +
            "already registered in the database.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully updated"),
            @ApiResponse(responseCode = "400", description = "Bad request. Floor or ESP32's position doesn't exist"),
            @ApiResponse(responseCode = "404", description = "Not found. ESP32 doesn't exist")
    })
    @RequestMapping(method = PUT, path = "/esp32/update", produces = "application/json")
    public ResponseEntity<ESP32ResponseEntity> updateESP32(
            @Parameter(description = "Http headers", required = true)
            @RequestHeader HttpHeaders httpHeaders,
            @Parameter(description = "ESP32 to update. Cannot be null or empty", required = true)
            @Valid @RequestBody ESP32Entity esp32Entity, Errors errors) {

        ESP32ResponseEntity esp32ReponseEntity = catchErrors(httpHeaders, esp32Entity.getMacAddress(), errors);

        if (esp32ReponseEntity != null) {
            return ResponseEntity.status(esp32ReponseEntity.getCode()).body(esp32ReponseEntity);
        }

        ESP32Entity esp32Exist = esp32Repository.findByMacAddress(esp32Entity.getMacAddress());
        if (esp32Exist == null) {
            esp32ReponseEntity = new ESP32ResponseEntity(HttpStatus.NOT_FOUND);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(esp32ReponseEntity);
        }

        FloorEntity floorEntity = this.floorRepository.findByName(httpHeaders.getFirst("esp32-floor"));
        if (floorEntity == null) {
            esp32ReponseEntity = new ESP32ResponseEntity(HttpStatus.BAD_REQUEST);
            esp32ReponseEntity.getErrors().add("Bad Request. Floor doesn't exist");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(esp32ReponseEntity);
        }

        PositionEntity positionEntity = this.positionRepository.findByName(httpHeaders.getFirst("esp32-position"));
        if (positionEntity == null) {
            esp32ReponseEntity = new ESP32ResponseEntity(HttpStatus.BAD_REQUEST);
            esp32ReponseEntity.getErrors().add("Bad Request. Position doesn't exist");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(esp32ReponseEntity);
        }

        esp32Exist.setFloor(floorEntity);
        esp32Exist.setPosition(positionEntity);
        esp32Exist.setRssi(esp32Entity.getRssi());
        esp32Exist.setSsid(esp32Entity.getSsid());
        esp32Exist.setBssid(esp32Entity.getBssid());
        esp32Exist.setIpAddress(esp32Entity.getIpAddress());

        esp32Repository.save(esp32Exist);

        esp32ReponseEntity = new ESP32ResponseEntity(esp32Exist, HttpStatus.OK);
        return ResponseEntity.status(HttpStatus.OK).body(esp32ReponseEntity);
    }

    private ESP32ResponseEntity catchErrors(HttpHeaders httpHeaders, String macAddress, Errors errors) {
        ESP32ResponseEntity esp32ReponseEntity;

        if (httpHeaders.getFirst("esp32-floor") == null ||
                httpHeaders.getFirst("esp32-position") == null) {
            esp32ReponseEntity = new ESP32ResponseEntity(HttpStatus.BAD_REQUEST);
            esp32ReponseEntity.getErrors().add("Missing headers properties.");
            return esp32ReponseEntity;
        }

        if (errors.hasErrors()) {
            esp32ReponseEntity = new ESP32ResponseEntity(HttpStatus.BAD_REQUEST);
            esp32ReponseEntity.fromBindingErrors(errors);
            return esp32ReponseEntity;
        }

        ESP32AuthorizedEntity esp32AuthorizedEntity = esp32AuthorizedRepository.findByMacAddress(macAddress);
        if (esp32AuthorizedEntity == null) {
            esp32ReponseEntity = new ESP32ResponseEntity(HttpStatus.UNAUTHORIZED);
            return esp32ReponseEntity;
        }
        return null;
    }
}
