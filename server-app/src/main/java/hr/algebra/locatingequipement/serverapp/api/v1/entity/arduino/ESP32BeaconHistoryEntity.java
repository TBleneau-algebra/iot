package hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "esp32s_beacons_history")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt", "deletedAt"}, allowGetters = true)
public class ESP32BeaconHistoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "esp32_beacon_id")
    private ESP32BeaconEntity esp32Beacon;

    @NotNull
    @Column(name = "beacon_rssi")
    private int rssi;

    @NotNull
    @Column(name = "beacon_tx_power")
    private int txPower;

    @Column(name = "beacon_payload")
    private int payload;

    @Column(name = "beacon_payload_length")
    private int payloadLength;

    @Column(name = "beacon_distance")
    private double distance;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public ESP32BeaconHistoryEntity() {
    }

    public ESP32BeaconHistoryEntity(ESP32BeaconEntity esp32BeaconEntity) {
        this.createdAt = esp32BeaconEntity.getUpdatedAt();
        this.rssi = esp32BeaconEntity.getRssi();
        this.txPower = esp32BeaconEntity.getTxPower();
        this.payload = esp32BeaconEntity.getPayload();
        this.payloadLength = esp32BeaconEntity.getPayloadLength();
        this.distance = esp32BeaconEntity.getDistance();
        this.esp32Beacon = esp32BeaconEntity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ESP32BeaconEntity getEsp32Beacon() {
        return esp32Beacon;
    }

    public void setEsp32Beacon(ESP32BeaconEntity esp32Beacon) {
        this.esp32Beacon = esp32Beacon;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public int getTxPower() {
        return txPower;
    }

    public void setTxPower(int txPower) {
        this.txPower = txPower;
    }

    public int getPayload() {
        return payload;
    }

    public void setPayload(int payload) {
        this.payload = payload;
    }

    public int getPayloadLength() {
        return payloadLength;
    }

    public void setPayloadLength(int payloadLength) {
        this.payloadLength = payloadLength;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
