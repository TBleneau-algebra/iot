package hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "beacons")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)
public class BeaconEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @NotBlank
    @Column(name = "mac_address", nullable = false)
    private String macAddress;

    @Column(name = "address_type")
    private String addressType;

    @Column(name = "manufacturer_id")
    private int manufacturerId;

    @NotBlank
    @Column(name = "service_uuid", nullable = false)
    private String serviceUUID;

    @Column(name = "service_data")
    private String serviceData;

    @Column(name = "service_data_uuid")
    private String serviceDataUUID;

    @Column(name = "minor")
    private int minor;

    @Column(name = "major")
    private int major;

    @Column(name = "appearance")
    private int appearance;

    @JsonIgnore
    @OneToMany(mappedBy = "beacon", cascade = CascadeType.ALL)
    private Set<ESP32BeaconEntity> esp32s = new HashSet<>();

    @Column(name = "created_at", nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;

    public BeaconEntity() {
    }

    @Override
    public String toString() {
        return "ID : " + this.id + ", " +
                "NAME : " + this.name + ", " +
                "MAC ADDRESS : " + this.macAddress + ", " +
                "ADDRESS TYPE : " + this.addressType + ", " +
                "MANUFACTURER ID : " + this.manufacturerId + ", " +
                "SERVICE UUID : " + this.serviceUUID + ", " +
                "SERVICE DATA : " + this.serviceData + ", " +
                "SERVICE DATA UUID : " + this.serviceDataUUID + ", " +
                "APPEARANCE : " + this.appearance + ", " +
                "MINOR : " + this.minor + ", " +
                "MAJOR : " + this.major;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public int getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(int manufacturer) {
        this.manufacturerId = manufacturer;
    }

    public String getServiceUUID() {
        return serviceUUID;
    }

    public void setServiceUUID(String serviceUUID) {
        this.serviceUUID = serviceUUID;
    }

    public String getServiceData() {
        return serviceData;
    }

    public void setServiceData(String serviceData) {
        this.serviceData = serviceData;
    }

    public String getServiceDataUUID() {
        return serviceDataUUID;
    }

    public void setServiceDataUUID(String serviceDataUUID) {
        this.serviceDataUUID = serviceDataUUID;
    }

    public Set<ESP32BeaconEntity> getEsp32s() {
        return esp32s;
    }

    public void setEsp32s(Set<ESP32BeaconEntity> esp32s) {
        this.esp32s = esp32s;
    }

    public int getMajor() {
        return major;
    }

    public void setMajor(int major) {
        this.major = major;
    }

    public int getMinor() {
        return minor;
    }

    public void setMinor(int minor) {
        this.minor = minor;
    }

    public int getAppearance() {
        return appearance;
    }

    public void setAppearance(int appearance) {
        this.appearance = appearance;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
