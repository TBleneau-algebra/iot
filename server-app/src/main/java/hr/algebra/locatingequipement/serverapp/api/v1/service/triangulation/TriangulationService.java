package hr.algebra.locatingequipement.serverapp.api.v1.service.triangulation;

import hr.algebra.locatingequipement.serverapp.api.v1.entity.circle.CircleEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TriangulationService {

    private final TriangulationCoefficientService triangulationCoefficientService;

    public TriangulationService(TriangulationCoefficientService triangulationCoefficientService) {
        this.triangulationCoefficientService = triangulationCoefficientService;
    }

    public int isInCircle(double x, double y, CircleEntity circle) {
        int result = 0;
        int precision = 5;

        double eq1 = Math.sqrt(Math.pow((x - circle.getX()), 2) + Math.pow((y - circle.getY()), 2));
        double eq2 = Math.sqrt(Math.pow(circle.getRadius(), 2));
        double distance = Math.sqrt(Math.pow((eq1 - eq2), 2));

        while (distance < precision) {
            result = precision;
            precision -= 1;
        }
        return result;
    }

    public List<CircleEntity> triangulation(CircleEntity circle1, CircleEntity circle2, CircleEntity circle3) {
        double aCoefficient = this.triangulationCoefficientService.aCoefficient(circle1, circle2);
        double dCoefficient = this.triangulationCoefficientService.dCoefficient(circle1, circle2);
        double ACoefficient = this.triangulationCoefficientService.ACoefficient(dCoefficient);
        double BCoefficient = this.triangulationCoefficientService.BCoefficient(aCoefficient, dCoefficient, circle1);
        double CCoefficient = this.triangulationCoefficientService.CCoefficient(aCoefficient, circle1);
        double deltaCoefficient = this.triangulationCoefficientService.deltaCoefficient(ACoefficient, BCoefficient, CCoefficient);

        while (deltaCoefficient < 0) {
            circle1.setRadius(circle1.getRadius() + 1);
            circle2.setRadius(circle2.getRadius() + 1);
            aCoefficient = this.triangulationCoefficientService.aCoefficient(circle1, circle2);
            BCoefficient = this.triangulationCoefficientService.BCoefficient(aCoefficient, dCoefficient, circle1);
            CCoefficient = this.triangulationCoefficientService.CCoefficient(aCoefficient, circle1);
            deltaCoefficient = this.triangulationCoefficientService.deltaCoefficient(ACoefficient, BCoefficient, CCoefficient);
        }

        List<Double> X = this.triangulationCoefficientService.XCoefficient(deltaCoefficient, BCoefficient, ACoefficient);
        List<Double> Y = this.triangulationCoefficientService.YCoefficient(X, aCoefficient, dCoefficient);

        List<CircleEntity> circleEntities = new ArrayList<>();
        circleEntities.add(new CircleEntity(X.get(0), Y.get(0), 0, this.isInCircle(X.get(0), Y.get(0), circle3)));
        circleEntities.add(new CircleEntity(X.get(1), Y.get(1), 0, this.isInCircle(X.get(1), Y.get(1), circle3)));
        return circleEntities;
    }


    public CircleEntity closestIntersection(List<CircleEntity> circleEntities) {
        List<CircleEntity> solutions;

        if (circleEntities.size() >= 3) {

            if (circleEntities.get(0).getY() == circleEntities.get(1).getY()) {
                solutions = this.triangulation(circleEntities.get(2), circleEntities.get(1), circleEntities.get(0));
            } else {
                solutions = this.triangulation(circleEntities.get(0), circleEntities.get(1), circleEntities.get(2));
            }
            if ((solutions.get(0).getTrustIndex() == 0 && solutions.get(1).getTrustIndex() == 0) ||
                    ((solutions.get(0).getX() < 0 || solutions.get(0).getY() < 0) && (solutions.get(1).getX() < 0 || solutions.get(1).getY() < 0))) {
                return null;
            }
            if (solutions.get(0).getX() < 0 || solutions.get(0).getY() < 0) {
                return solutions.get(1);
            } else if (solutions.get(1).getX() < 0 || solutions.get(1).getY() < 0) {
                return solutions.get(0);
            }
            return (solutions.get(0).getTrustIndex() < solutions.get(1).getTrustIndex()) ? solutions.get(0) : solutions.get(1);
        }
        return null;
    }
}
