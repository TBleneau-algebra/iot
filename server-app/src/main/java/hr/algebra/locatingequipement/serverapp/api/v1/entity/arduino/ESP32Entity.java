package hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "esp32s")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)
public class ESP32Entity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotEmpty
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    @Column(name = "rssi", nullable = false)
    private String rssi;

    @NotEmpty
    @Column(name = "ssid", nullable = false)
    private String ssid;

    @NotEmpty
    @Column(name = "bssid", nullable = false)
    private String bssid;

    @NotEmpty
    @Column(name = "ip_address", nullable = false)
    private String ipAddress;

    @NotEmpty
    @Column(name = "mac_address", nullable = false)
    private String macAddress;

    @JsonIgnore
    @OneToMany(mappedBy = "esp32", cascade = CascadeType.ALL)
    private Set<ESP32BeaconEntity> beacons = new HashSet<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "floor_id")
    private FloorEntity floor;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "position_id")
    private PositionEntity position;

    @Column(name = "created_at", nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Column(name = "updated_at", nullable = false)
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    public ESP32Entity() {
    }

    public ESP32Entity(String rssi, String ssid, String bssid, String ipAddress, String macAddress) {
        this.rssi = rssi;
        this.ssid = ssid;
        this.bssid = bssid;
        this.ipAddress = ipAddress;
        this.macAddress = macAddress;
    }

    @Override
    public String toString() {
        return "ID : " + this.id + ", " +
                "RSSI : " + this.rssi + ", " +
                "SSID : " + this.ssid + ", " +
                "BSSID : " + this.bssid + ", " +
                "IP ADDRESS : " + this.ipAddress + ", " +
                "WIFI MAC ADDRESS : " + this.macAddress;
    }

    public Long getId() {
        return id;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getRssi() {
        return rssi;
    }

    public void setRssi(String rssi) {
        this.rssi = rssi;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Set<ESP32BeaconEntity> getBeacons() {
        return beacons;
    }

    public void setBeacons(Set<ESP32BeaconEntity> beacons) {
        this.beacons = beacons;
    }

    public FloorEntity getFloor() {
        return floor;
    }

    public void setFloor(FloorEntity floor) {
        this.floor = floor;
    }

    public PositionEntity getPosition() {
        return position;
    }

    public void setPosition(PositionEntity position) {
        this.position = position;
    }
}
