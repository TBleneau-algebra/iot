package hr.algebra.locatingequipement.serverapp.api.v1.repository;

import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.BeaconEntity;
import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.ESP32BeaconEntity;
import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.ESP32Entity;
import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.FloorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ESP32BeaconRepository extends JpaRepository<ESP32BeaconEntity, Long> {

    ESP32BeaconEntity findByEsp32AndBeacon(ESP32Entity esp32Entity, BeaconEntity beaconEntity);

    List<ESP32BeaconEntity> findAllByEsp32FloorAndBeacon(FloorEntity floorEntity, BeaconEntity beaconEntity);

    List<ESP32BeaconEntity> findAllByEsp32Floor(FloorEntity floorEntity);
}
