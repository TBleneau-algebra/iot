package hr.algebra.locatingequipement.serverapp.api.v1.service.triangulation;

import hr.algebra.locatingequipement.serverapp.api.v1.entity.circle.CircleEntity;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class TriangulationCoefficientService {

    public double aCoefficient(CircleEntity circle1, CircleEntity circle2) {
        return (-Math.pow(circle1.getX(), 2) - Math.pow(circle1.getY(), 2) +
                Math.pow(circle2.getX(), 2) + Math.pow(circle2.getY(), 2) +
                Math.pow(circle1.getRadius(), 2) - Math.pow(circle2.getRadius(), 2)) /
                (2 * (circle2.getY() - circle1.getY()));
    }

    public double dCoefficient(CircleEntity circle1, CircleEntity circle2) {
        return (circle2.getX() - circle1.getX()) / (circle2.getY() - circle1.getY());
    }

    public double ACoefficient(double dCoefficient) {
        return Math.pow(dCoefficient, 2) + 1;
    }

    public double BCoefficient(double aCoefficient, double dCoefficient, CircleEntity circle1) {
        return (-2 * circle1.getX()) + (2 * circle1.getY() * dCoefficient) - (2 * aCoefficient * dCoefficient);
    }

    public double CCoefficient(double aCoefficient, CircleEntity circle1) {
        return Math.pow(circle1.getX(), 2) + Math.pow(circle1.getY(), 2) - (2 * circle1.getY() * aCoefficient) +
                Math.pow(aCoefficient, 2) - Math.pow(circle1.getRadius(), 2);
    }

    public double deltaCoefficient(double ACoefficient, double BCoefficient, double CCoefficient) {
        return Math.pow(BCoefficient, 2) - 4 * ACoefficient * CCoefficient;
    }

    public List<Double> XCoefficient(double deltaCoefficient, double BCoefficient, double ACoefficient) {
        double x11 = (-BCoefficient + Math.sqrt(deltaCoefficient)) / (2 * ACoefficient);
        double x12 = (-BCoefficient - Math.sqrt(deltaCoefficient)) / (2 * ACoefficient);

        return Arrays.asList(x11, x12);
    }

    public List<Double> YCoefficient(List<Double> X1Coefficient, double aCoefficient, double dCoefficient) {
        double y11 = aCoefficient - (X1Coefficient.get(0) * dCoefficient);
        double y12 = aCoefficient - (X1Coefficient.get(1) * dCoefficient);

        return Arrays.asList(y11, y12);
    }
}
