package hr.algebra.locatingequipement.serverapp.api.v1.repository;

import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.FloorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FloorRepository extends JpaRepository<FloorEntity, Long> {
    FloorEntity findByName(String name);
}
