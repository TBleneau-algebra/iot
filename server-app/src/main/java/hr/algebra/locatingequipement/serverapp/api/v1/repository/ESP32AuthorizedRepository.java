package hr.algebra.locatingequipement.serverapp.api.v1.repository;

import hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino.ESP32AuthorizedEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ESP32AuthorizedRepository extends JpaRepository<ESP32AuthorizedEntity, Long> {

    ESP32AuthorizedEntity findByMacAddress(String macAddress);
}
