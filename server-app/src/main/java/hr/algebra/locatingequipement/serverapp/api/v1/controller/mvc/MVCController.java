package hr.algebra.locatingequipement.serverapp.api.v1.controller.mvc;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class MVCController implements ErrorController {

    public MVCController() {
    }

    @GetMapping("/")
    public String home() {
        return "layouts/home.layout";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping("/error")
    public String handleError() {
        return "layouts/error.layout";
    }

}
