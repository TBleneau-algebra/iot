package hr.algebra.locatingequipement.serverapp.api.v1.entity.arduino;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "floors")
@EntityListeners(AuditingEntityListener.class)
public class FloorEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotEmpty
    @Column(name = "name")
    private String name;

    @NotEmpty
    @Column(name = "x_max")
    private double xMax;

    @NotEmpty
    @Column(name = "y_max")
    private double yMax;

    @JsonIgnore
    @OneToMany(mappedBy = "floor", cascade = CascadeType.ALL)
    private Set<ESP32Entity> esp32s = new HashSet<>();

    public FloorEntity() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<ESP32Entity> getEsp32s() {
        return esp32s;
    }

    public void setEsp32s(Set<ESP32Entity> esp32s) {
        this.esp32s = esp32s;
    }

    public double getyMax() {
        return yMax;
    }

    public void setyMax(double yMax) {
        this.yMax = yMax;
    }

    public double getxMax() {
        return xMax;
    }

    public void setxMax(double xMax) {
        this.xMax = xMax;
    }
}
