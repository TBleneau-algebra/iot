package hr.algebra.locatingequipement.serverapp.api.v1.entity.circle;

public class CircleEntity {

    private double x;
    private double y;
    private double radius;
    private int trustIndex;

    public CircleEntity() {

    }

    public CircleEntity(double x, double y, double radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.trustIndex = 0;
    }

    public CircleEntity(double x, double y, double radius, int trustIndex) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.trustIndex = trustIndex;
    }

    @Override
    public String toString() {
        return "x = " + this.x + "  , y = " + this.y + "  , radius = " + this.radius +
                "  , trustIndex = " + this.trustIndex;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public int getTrustIndex() {
        return trustIndex;
    }

    public void setTrustIndex(int trustIndex) {
        this.trustIndex = trustIndex;
    }
}
