CREATE TABLE IF NOT EXISTS `beacons`
(
    `id`                int(11)      NOT NULL AUTO_INCREMENT,
    `name`              varchar(255),
    `mac_address`       varchar(255) NOT NULL,
    `address_type`      varchar(255) NOT NULL,
    `manufacturer_id`   int(11),
    `service_uuid`      varchar(255) NOT NULL,
    `service_data`      varchar(255),
    `service_data_uuid` varchar(255),
    `appearance`        int(11),
    `minor`             int(11),
    `major`             int(11),
    `created_at`        DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE = INNODB;
