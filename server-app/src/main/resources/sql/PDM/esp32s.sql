CREATE TABLE IF NOT EXISTS `esp32s`
(
    `id`          int(11)      NOT NULL AUTO_INCREMENT,
    `rssi`        varchar(255) NOT NULL,
    `ssid`        varchar(255) NOT NULL,
    `bssid`       varchar(255) NOT NULL,
    `ip_address`  varchar(255) NOT NULL,
    `mac_address` varchar(255) NOT NULL,
    `created_at`  DATETIME DEFAULT CURRENT_TIMESTAMP,
    `updated_at`  DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `floor_id`    int(11)      NOT NULL,
    `position_id` int(11)      NOT NULL,
    FOREIGN KEY (`floor_id`) REFERENCES `floors` (`id`),
    FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`),
    PRIMARY KEY (`id`)
) ENGINE = INNODB;
