CREATE TABLE `positions` (`id` int(11) NOT NULL AUTO_INCREMENT, `name` varchar(255) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=INNODB;

INSERT INTO `positions` (`id`, `name`) VALUES (1, 'CORNER_BOTTOM_RIGHT');

INSERT INTO `positions` (`id`, `name`) VALUES (2, 'CORNER_TOP_RIGHT');

INSERT INTO `positions` (`id`, `name`) VALUES (3, 'CORNER_BOTTOM_LEFT');

INSERT INTO `positions` (`id`, `name`) VALUES (4, 'CORNER_TOP_LEFT');

