CREATE TABLE `authorized_esp32` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`mac_address` varchar(255) DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=INNODB;

INSERT INTO `authorized_esp32` (`id`, `mac_address`) VALUES (1, '3C:71:BF:7D:69:B4');

INSERT INTO `authorized_esp32` (`id`, `mac_address`) VALUES (2, 'A4:CF:12:6B:50:40');

INSERT INTO `authorized_esp32` (`id`, `mac_address`) VALUES (3, '3C:71:BF:7D:79:7C');

INSERT INTO `authorized_esp32` (`id`, `mac_address`) VALUES (4, 'A4:CF:12:6B:2E:7C');
