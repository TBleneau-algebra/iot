CREATE TABLE `esp32s_beacons` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `esp32_id` int(11) NOT NULL,
    `beacon_id` int(11) NOT NULL,
    `beacon_rssi` int(11) NOT NULL,
    `beacon_tx_power` int(11) NOT NULL,
    `beacon_payload` int(11) NOT NULL,
    `beacon_payload_length` int(11) NOT NULL,
    `beacon_distance` double NOT NULL,
    `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
    `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (`esp32_id`) REFERENCES `esp32s`(`id`),
    FOREIGN KEY (`beacon_id`) REFERENCES `beacons`(`id`),
    PRIMARY KEY (`id`)
) ENGINE=INNODB;
