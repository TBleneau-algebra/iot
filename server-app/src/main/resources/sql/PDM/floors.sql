CREATE TABLE `floors` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(255) DEFAULT NULL,
        `x_max` float NOT NULL,
        `y_max` float NOT NULL,
        PRIMARY KEY (`id`)
) ENGINE=INNODB;

INSERT INTO `floors` (`id`, `name`, `x_max`, `y_max`) VALUES (1, 'FLOOR_ALGEBRA_LAB', 7.00, 6.21);
