CREATE TABLE `esp32s_beacons_history` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `esp32_beacon_id` int(11) NOT NULL,
    `beacon_rssi` int(11) NOT NULL,
    `beacon_tx_power` int(11) NOT NULL,
    `beacon_payload` int(11) NOT NULL,
    `beacon_payload_length` int(11) NOT NULL,
    `beacon_distance` double NOT NULL,
    `created_at` DATETIME DEFAULT '1970-01-01 00:00:01',
    FOREIGN KEY (`esp32_beacon_id`) REFERENCES `esp32s_beacons`(`id`),
    PRIMARY KEY (`id`)
) ENGINE=INNODB;

