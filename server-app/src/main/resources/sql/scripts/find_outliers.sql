WITH orderedList AS (
    SELECT id, esp32_beacon_id, beacon_rssi, beacon_distance, ROW_NUMBER() OVER (ORDER BY beacon_rssi desc) AS row_n
    FROM esp32s_beacons_history
    WHERE esp32_beacon_id = ?1
      AND created_at >= ?2),
     iqrange (id, beacon_rssi, esp32_beacon_id, beacon_distance, q_three, q_one, outlier_range) AS (
         SELECT id,
                beacon_rssi,
                esp32_beacon_id,
                beacon_distance,
                (
                    SELECT beacon_rssi AS quartile_break
                    FROM orderedList
                    WHERE row_n = FLOOR((SELECT COUNT(*)
                                         FROM orderedList) * 0.75)
                )     AS q_three,
                (
                    SELECT beacon_rssi AS quartile_break
                    FROM orderedList
                    WHERE row_n = FLOOR((SELECT COUNT(*)
                                         FROM orderedList) * 0.25)
                )     AS q_one,
                1.05 * ((
                            SELECT beacon_rssi AS quartile_break
                            FROM orderedList
                            WHERE row_n = FLOOR((SELECT COUNT(*)
                                                 FROM orderedList) * 0.75)
                        ) - (
                            SELECT beacon_rssi AS quartile_break
                            FROM orderedList
                            WHERE row_n = FLOOR((SELECT COUNT(*)
                                                 FROM orderedList) * 0.25)
                        )
                    ) AS outlier_range
         FROM orderedList
     ),
     bounds as (
         SELECT id, beacon_rssi, MAX(q_three) + MAX(outlier_range) AS UPPER, MAX(q_one) - MAX(outlier_range) AS LOWER
         FROM iqrange
         GROUP BY id, beacon_rssi
     )
select id
FROM bounds
WHERE beacon_rssi >= LOWER
   OR beacon_rssi <= UPPER;
