# Internet of Things - Locating Equipment Project

### Mobile project

If you would like more information about the application client, follow this link :
[README.md](./ionic-app/README.md)

### Server-App project

If you would like more information about the application API, follow this link :
[README.md](./server-app/README.md)

### SQL Database information

If you would like more information about the application SQL database, follow this link :
[README.md](./sql/README.md)

