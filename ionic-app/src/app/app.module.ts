/**
 * Import of Angular and Ionic modules
 */
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';
import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {ScreenOrientation} from '@ionic-native/screen-orientation/ngx';
/**
 * Import of application's modules
 */
import {AppRoutingModule} from './app-routing.module';
import {PagesModule} from './pages/pages.module';
/**
 * Import of application's components
 */
import {AppComponent} from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        PagesModule
    ],
    providers: [
        StatusBar,
        SplashScreen,
        ScreenOrientation,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}
