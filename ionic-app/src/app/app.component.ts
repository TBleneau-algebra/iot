import {Component, OnInit} from '@angular/core';
import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {ScreenOrientation} from '@ionic-native/screen-orientation/ngx';

@Component({
    selector: 'app-root',
    template: '<ion-app>\n' +
        '    <ion-router-outlet></ion-router-outlet>\n' +
        '</ion-app>\n'
})
export class AppComponent implements OnInit {

    /**
     * @description Constructor of AppComponent component
     *
     * The constructor creates an instance of the AppComponent component and specifies the default values of
     * the input and output variables of the component.
     *
     * @param platform The Platform service can be used to get information about your current device
     * @param splashScreen This plugin displays and hides a splash screen during application launch
     * @param statusBar Manage the appearance of the native status bar.
     * @param screenOrientation Cordova plugin to set/lock the screen orientation in a common way.
     */
    constructor(private platform: Platform, private splashScreen: SplashScreen,
                private statusBar: StatusBar, private screenOrientation: ScreenOrientation) {
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.initializeApp();
    }

    /**
     * @description This method is used to initialize the application
     */
    initializeApp() {
        const parent = this;

        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT).then(() => {
        });

        this.platform.ready().then(() => {
            setTimeout(function() {
                parent.statusBar.styleLightContent();
                parent.statusBar.backgroundColorByHexString('#33000000');
                parent.splashScreen.hide();
            }, 1000);
        });
    }
}
