/**
 * Import of Angular and Ionic module
 */
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
/**
 * Import of application's component
 */
import {AboutUsComponent} from './about-us.component';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        RouterModule,
    ],
    declarations: [AboutUsComponent]
})
export class AboutUsModule {
}
