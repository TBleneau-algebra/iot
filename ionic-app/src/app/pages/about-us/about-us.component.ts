import {Component} from '@angular/core';
import {ToolbarService} from '../../shared/services/toolbar/toolbar.service';

@Component({
    selector: 'app-about-us-pages',
    templateUrl: 'about-us.component.html'
})
export class AboutUsComponent {

    /**
     * @description Constructor of AboutUsComponent component
     *
     * The constructor creates an instance of the AboutUsComponent component and specifies the default values of
     * the input and output variables of the component.
     *
     * @param toolbarService A reference to the toolbarService of the application
     */
    constructor(private toolbarService: ToolbarService) {
    }

    /**
     * @description In addition to the Angular life cycle events, Ionic Angular provides a few additional events.
     *
     * Fired when the component routing to is about to animate into view.
     * The difference between ionViewWillEnter and ionViewDidEnter is when they fire
     */
    ionViewWillEnter(): void {
        this.toolbarService.title = 'About Us';
        this.toolbarService.clear();
    }
}
