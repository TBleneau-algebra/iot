import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {ToolbarService} from '../../shared/services/toolbar/toolbar.service';
import {EquipmentService} from '../../shared/services/equipment/equipment.service';
import {AlertService} from '../../shared/services/alert/alert.service';
import {environment} from '../../../environments/environment';

@Component({
    selector: 'app-rooms-pages',
    templateUrl: 'api-entry-point.component.html'
})
export class ApiEntryPointComponent {

    /**
     * @description Constructor of ApiEntryPointComponent component
     *
     * The constructor creates an instance of the ApiEntryPointComponent component and specifies the default values of
     * the input and output variables of the component.
     *
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     * @param toolbarService A reference to the ToolbarService of the application
     * @param equipmentService A reference to the EquipmentService of the application
     * @param alertService A reference to the alertService of the application
     */
    constructor(private router: Router, private toolbarService: ToolbarService,
                private equipmentService: EquipmentService, private alertService: AlertService) {
    }

    /**
     * @description In addition to the Angular life cycle events, Ionic Angular provides a few additional events.
     *
     * Fired when the component routing to is about to animate into view.
     * The difference between ionViewWillEnter and ionViewDidEnter is when they fire
     */
    ionViewWillEnter(): void {
        this.toolbarService.title = 'API entry point selection';
        this.toolbarService.clear();
    }

    /**
     * @description This method allows you to change the entry point of connection to the API
     *
     * @param entry Choice of entry point 'local' | 'server'
     */
    select(entry: string): void {
        this.alertService.question('Enter the API entry point, for example localhost:8080 or 172.20.9.212:8080')
            .subscribe(response => {
                if (response && response !== '') {
                    environment.apiHost = 'http://' + response + '/api/v1';
                    this.equipmentService.apiHost = 'http://' + response + '/api/v1';
                }
            });

    }

    /**
     * @description This method returns the current API connection entry point used by the Http request service
     */
    apiHost(): string {
        return this.equipmentService.apiHost;
    }
}
