/**
 * Import of Angular and Ionic modules
 */
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
/**
 * Import of application's component
 */
import {ApiEntryPointComponent} from './api-entry-point.component';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        RouterModule,
    ],
    declarations: [ApiEntryPointComponent]
})
export class ApiEntryPointModule {
}
