/**
 * Import of Angular and Ionic modules
 */
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
/**
 * Import of application's component
 */
import {BeaconsComponent} from './beacons.component';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        RouterModule.forChild([{path: '', component: BeaconsComponent}])
    ],
    declarations: [BeaconsComponent]
})
export class BeaconsModule {
}
