import {Component, Input} from '@angular/core';
import {ToolbarService} from '../../shared/services/toolbar/toolbar.service';
import {ActivatedRoute, Router} from '@angular/router';
import {RequestResponseInterface} from '../../shared/services/request/response/request-response.interface';
import {EquipmentService} from '../../shared/services/equipment/equipment.service';
import {AlertService} from '../../shared/services/alert/alert.service';
import {RequestParamsInterface} from '../../shared/services/request/params/request-params.interface';
import {RequestParamsClass} from '../../shared/services/request/params/request-params.class';

@Component({
    selector: 'app-beacons-pages',
    templateUrl: 'beacons.component.html',
})
export class BeaconsComponent {

    /**
     * @description Mode to switch between an icon for the beacon or an icon for a specific object
     */
    @Input() public modeBeacon: boolean;

    /**
     * @description List of retrieved beacons via the API
     */
    private _beacons: Array<any>;

    /**
     * @description Selected room's id
     */
    private _roomId: string;

    /**
     * @description Constructor of BeaconsComponent component
     *
     * The constructor creates an instance of the BeaconsComponent component and specifies the default values of
     * the input and output variables of the component.
     *
     * @param activatedRoute Provides access to information about a route associated with a component that is loaded in an outlet
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     * @param toolbarService A reference to the ToolbarService of the application
     * @param equipmentService A reference to the EquipmentService of the application
     * @param alertService A reference to the alertService of the application
     */
    constructor(private activatedRoute: ActivatedRoute, private router: Router,
                private toolbarService: ToolbarService,
                private equipmentService: EquipmentService, private alertService: AlertService) {
        this.beacons = [];
    }

    /**
     * @description In addition to the Angular life cycle events, Ionic Angular provides a few additional events.
     *
     * Fired when the component routing to is about to animate into view.
     * The difference between ionViewWillEnter and ionViewDidEnter is when they fire
     */
    ionViewWillEnter(): void {
        this.modeBeacon = false;
        this.toolbarService.title = 'Objects List';
        this.toolbarService.previousUrl = '/pages/room';

        this.activatedRoute.paramMap.subscribe(params => {
            if (!params.get('roomId')) {
                this.router.navigate(['/']).then(() => {
                });
            }
            this.roomId = params.get('roomId');
            this.list();
        });
    }

    /**
     * @description This method allows to navigate on the page containing the map of the room selected
     * with the id of the beacon passed in parameter to the Angular router
     *
     * @param id Beacon's id
     */
    navigateTo(id: string) {
        const route: string = this.router.url;

        this.router.navigate([this.router.url + '/beacon/' + id], {replaceUrl: true}).then(() => {
        });
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method GET of the RequestService service
     */
    list(): void {
        const params: RequestParamsInterface = new RequestParamsClass();

        params.params.push({name: 'floorId', value: this.roomId});
        this.equipmentService.get('/beacon', null, params).then((response: RequestResponseInterface) => {
            (response.responseStatus === 200) ? this.success(response.responseData) : this.error(response.responseData);
        })
            .catch((error: RequestResponseInterface) => {
                this.error(error.responseError);
            });
    }

    /**
     * @description This method is called if the http request has been executed successfully.
     *
     * @param data List of beacons
     */
    private success(data: any): void {
        this.beacons = (data.hasOwnProperty('data')) ? data.data : [];

        if (this.beacons.length === 0) {
            this.alertService.error('No objects were found');
        }
    }

    /**
     * @description This method is called if the http request was executed with errors
     *
     * @param data Errors received via the API
     */
    private error(data: any): void {
        this.beacons = [];

        if (data.hasOwnProperty('errors')) {
            for (const item of (data.errors as Array<any>)) {
                this.alertService.error(item);
            }
        } else {
            this.alertService.error('API connection problem');
        }
    }

    /**
     * @description This method allows you to retrieve the selected room's id
     */
    get roomId(): string {
        return this._roomId;
    }

    /**
     * @description This method allows you to assign the selected room's id
     *
     * @param value New selected room's id
     */
    set roomId(value: string) {
        this._roomId = value;
    }

    /**
     * @description This method allows you to retrieve the list of beacons available in the selected room
     */
    get beacons(): Array<any> {
        return this._beacons;
    }

    /**
     * @description This method allows you to assign the list of beacons available in the selected room
     *
     * @param value New list of beacons available in the selected room
     */
    set beacons(value: Array<any>) {
        this._beacons = value;
    }
}
