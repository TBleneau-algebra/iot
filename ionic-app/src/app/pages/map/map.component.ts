import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ToolbarService} from '../../shared/services/toolbar/toolbar.service';
import {RequestResponseInterface} from '../../shared/services/request/response/request-response.interface';
import {EquipmentService} from '../../shared/services/equipment/equipment.service';
import {AlertService} from '../../shared/services/alert/alert.service';
import {RequestParamsInterface} from '../../shared/services/request/params/request-params.interface';
import {RequestParamsClass} from '../../shared/services/request/params/request-params.class';
import {floorData} from '../../shared/data/floor.data';
import {MapAxisClass} from './map-axis.class';

@Component({
    selector: 'app-map-pages',
    templateUrl: 'map.component.html'
})
export class MapComponent {

    /**
     * @description Name of the selected room
     */
    private _name: string;

    /**
     * @description Selected beacon's id
     */
    private beaconId: string;

    /**
     * @description Selected room's id
     */
    private roomId: string;

    /**
     * @description Minimum and maximum x-axis of the selected room
     */
    private _xAxis: { xMin: MapAxisClass, xMax: MapAxisClass };

    /**
     * @description Minimum and maximum y-axis of the selected room
     */
    private _yAxis: { yMin: MapAxisClass, yMax: MapAxisClass };

    /**
     * @description Marker to visualize the beacon in the room
     */
    private mapMarker: HTMLElement;

    /**
     * @description Constructor of MapComponent component
     *
     * The constructor creates an instance of the MapComponent component and specifies the default values of
     * the input and output variables of the component.
     *
     * @param activatedRoute Provides access to information about a route associated with a component that is loaded in an outlet
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     * @param toolbarService A reference to the ToolbarService of the application
     * @param equipmentService A reference to the EquipmentService of the application
     * @param alertService A reference to the alertService of the application
     */
    constructor(private activatedRoute: ActivatedRoute, private router: Router,
                private toolbarService: ToolbarService,
                private equipmentService: EquipmentService, private alertService: AlertService) {
        this.name = 'default';
        this.xAxis = {xMin: new MapAxisClass(), xMax: new MapAxisClass()};
        this.yAxis = {yMin: new MapAxisClass(), yMax: new MapAxisClass()};
    }

    /**
     * @description In addition to the Angular life cycle events, Ionic Angular provides a few additional events.
     *
     * Fired when the component routing to is about to animate into view.
     * The difference between ionViewWillEnter and ionViewDidEnter is when they fire
     */
    ionViewWillEnter(): void {
        this.toolbarService.title = 'Map';

        this.activatedRoute.paramMap.subscribe(params => {
            if (!params.get('beaconId') && !params.get('roomId')) {
                this.router.navigate(['/']).then(() => {
                });
            }
            this.beaconId = params.get('beaconId');
            this.roomId = params.get('roomId');

            this.toolbarService.previousUrl = '/pages/room/' + this.roomId;

            this.mapMarker = document.getElementById('map-marker');

            this.floorCoordinates();
            this.mapCoordinates();
            this.markerCoordinates();
        });
    }

    /**
     * @description
     */
    markerCoordinates(): void {
        const params: RequestParamsInterface = new RequestParamsClass();

        params.params.push({name: 'floorId', value: this.roomId});
        params.params.push({name: 'beaconId', value: this.beaconId});

        this.equipmentService.get('/beacon/position', null, params).then((response: RequestResponseInterface) => {
            (response.responseStatus === 200) ? this.calculateCoordinates(response.responseData.data) : this.error(response.responseData);
        })
            .catch((error: RequestResponseInterface) => {
                this.error(error.responseError);
            });
    }

    /**
     * @description This method returns the location of the asset corresponding to the selected room
     */
    sourceMap(): string {
        return '/assets/img/floors/' + this.name + '.png';
    }

    /**
     * @description This method retrieves the coordinates of the map to display the image of the selected room
     */
    private mapCoordinates(): void {
        const card: HTMLElement = document.getElementById('map-content');

        this.xAxis.xMin.map = 10;
        this.yAxis.yMin.map = 10;
        this.xAxis.xMax.map = card.offsetHeight - (this.xAxis.xMin.map + 20);
        this.yAxis.yMax.map = card.offsetWidth - (this.yAxis.yMin.map + 20);
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     *
     * It calls the method GET of the RequestService service
     */
    private floorCoordinates(): void {
        this.equipmentService.get('/floor/' + this.roomId).then((response: RequestResponseInterface) => {
            this.xAxis.xMax.floor = (response.responseStatus === 200) ? response.responseData.xMax : this.xAxis.xMax.floor;
            this.yAxis.yMax.floor = (response.responseStatus === 200) ? response.responseData.yMax : this.yAxis.yMax.floor;

            this.name = response.responseData.name;
            this.toolbarService.title = (response.responseStatus === 200) ? floorData[this.name] : 'Map';
        })
            .catch((error: RequestResponseInterface) => {
                this.error(error.responseError);
            });
    }

    /**
     * @description This method is called if the http request has been executed successfully.
     * It calculates the new coordinates of the marker
     *
     * @param data New marker's coordinates
     */
    private calculateCoordinates(data: any): void {
        let x: number = this.xAxis.xMin.map;
        let y: number = this.yAxis.yMin.map;

        if (data && data.hasOwnProperty('x') && data.hasOwnProperty('y')) {
            x = (data.x * this.xAxis.xMax.map) / this.xAxis.xMax.floor;
            y = (data.y * this.yAxis.yMax.map) / this.yAxis.yMax.floor;
        }
        this.mapMarker.style.top = (this.xAxis.xMax.map - x) + 'px';
        this.mapMarker.style.left = (this.yAxis.yMax.map - y) + 'px';
    }

    /**
     * @description This method is called if the http request was executed with errors
     *
     * @param data Errors received via the API
     */
    private error(data: any): void {
        if (data && data.hasOwnProperty('errors')) {
            for (const item of (data.errors as Array<any>)) {
                this.alertService.error(item);
            }
        } else {
            this.alertService.error('Error occurred. API connection problem');
        }
    }

    /**
     * @description This method allows you to retrieve the minimum and maximum x-axis of the selected room
     */
    get xAxis(): { xMin: MapAxisClass; xMax: MapAxisClass } {
        return this._xAxis;
    }

    /**
     * @description This method allows you to assign the minimum and maximum x-axis of the selected room
     *
     * @param value New minimum and maximum x-axis of the selected room
     */
    set xAxis(value: { xMin: MapAxisClass; xMax: MapAxisClass }) {
        this._xAxis = value;
    }

    /**
     * @description This method allows you to retrieve the minimum and maximum y-axis of the selected room
     */
    get yAxis(): { yMin: MapAxisClass; yMax: MapAxisClass } {
        return this._yAxis;
    }

    /**
     * @description This method allows you to assign the minimum and maximum y-axis of the selected room
     *
     * @param value New minimum and maximum y-axis of the selected room
     */
    set yAxis(value: { yMin: MapAxisClass; yMax: MapAxisClass }) {
        this._yAxis = value;
    }

    /**
     * @description This method allows you to retrieve the name of the selected room
     */
    get name(): string {
        return this._name;
    }

    /**
     * @description This method allows you to assign the name of the selected room
     *
     * @param value New name of the selected room
     */
    set name(value: string) {
        this._name = value;
    }
}
