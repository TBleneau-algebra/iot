/**
 * @description This class wrap the coordinates (x, y) of the selected room with the coordinates (x, y) of the HTML container
 * that displays the image of the room.
 */
export class MapAxisClass {
    floor: number;
    map: number;

    constructor() {
        this.floor = 0;
        this.map = 0;
    }
}
