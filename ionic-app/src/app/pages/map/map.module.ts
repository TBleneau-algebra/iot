/**
 * Import of Angular and Ionic modules
 */
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
/**
 * Import of application's components
 */
import {MapComponent} from './map.component';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        RouterModule,
    ],
    declarations: [MapComponent]
})
export class MapModule {
}
