/**
 * Import of Angular and Ionic modules
 */
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
/**
 * Import of application's component
 */
import {RoomsComponent} from './rooms.component';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        RouterModule,
    ],
    declarations: [RoomsComponent]
})
export class RoomsModule {
}
