import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {ToolbarService} from '../../shared/services/toolbar/toolbar.service';
import {EquipmentService} from '../../shared/services/equipment/equipment.service';
import {AlertService} from '../../shared/services/alert/alert.service';
import {RequestResponseInterface} from '../../shared/services/request/response/request-response.interface';
import {floorData} from '../../shared/data/floor.data';

@Component({
    selector: 'app-rooms-pages',
    templateUrl: 'rooms.component.html'
})
export class RoomsComponent {

    /**
     * @description List of retrieved rooms via the API
     */
    private _rooms: Array<any>;

    /**
     * @description Constructor of ApiEntryPointComponent component
     *
     * The constructor creates an instance of the ApiEntryPointComponent component and specifies the default values of
     * the input and output variables of the component.
     *
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     * @param toolbarService A reference to the ToolbarService of the application
     * @param equipmentService A reference to the EquipmentService of the application
     * @param alertService A reference to the alertService of the application
     */
    constructor(private router: Router, private toolbarService: ToolbarService,
                private equipmentService: EquipmentService, private alertService: AlertService) {
        this.rooms = [];
    }

    /**
     * @description In addition to the Angular life cycle events, Ionic Angular provides a few additional events.
     *
     * Fired when the component routing to is about to animate into view.
     * The difference between ionViewWillEnter and ionViewDidEnter is when they fire
     */
    ionViewWillEnter(): void {
        this.toolbarService.title = 'Rooms List';
        this.toolbarService.clear();
        this.list();
    }

    /**
     * @description This method allows to navigate on the page containing the list of beacons present in the room
     * whose id is passed in parameter to the Angular router.
     *
     * @param id Room's id
     */
    navigateTo(id: string) {
        this.router.navigate([this.router.url + '/' + id], {replaceUrl: true}).then(() => {
        });
    }

    /**
     * @description The method performs the Http request according to the parameters passed to it.
     * It calls the method GET of the RequestService service
     */
    list(): void {
        this.equipmentService.get('/floor').then((response: RequestResponseInterface) => {
            (response.responseStatus === 200) ? this.success(response.responseData) : this.error(response.responseData);
        })
            .catch((error: RequestResponseInterface) => {
                this.error(error.responseError);
            });
    }

    /**
     * @description This method is called if the http request has been executed successfully.
     *
     * @param data List of rooms
     */
    private success(data: any): void {
        this.rooms = (data.hasOwnProperty('data')) ? data.data : [];

        this.rooms.forEach(item => {
            item.name = (floorData[item.name]) ? floorData[item.name] : item.name;
        });

        if (this.rooms.length === 0) {
            this.alertService.error('No rooms were found');
        }
    }

    /**
     * @description This method is called if the http request was executed with errors
     *
     * @param data Errors received via the API
     */
    private error(data: any): void {
        this.rooms = [];

        if (data.hasOwnProperty('errors')) {
            for (const item of (data.errors as Array<any>)) {
                this.alertService.error(item);
            }
        } else {
            this.alertService.error('API connection problem');
        }
    }

    /**
     * @description This method allows you to retrieve the list of rooms available
     */
    get rooms(): Array<any> {
        return this._rooms;
    }

    /**
     * @description This method allows you to assign the list of rooms available
     *
     * @param value New list of rooms available
     */
    set rooms(value: Array<any>) {
        this._rooms = value;
    }
}
