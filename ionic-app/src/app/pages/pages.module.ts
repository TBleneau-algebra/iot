/**
 * Import of Angular and Ionic modules
 */
import {IonicModule} from '@ionic/angular';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
/**
 * Import of application's modules
 */
import {PagesRoutingModule} from './pages-routing.module';
import {AboutUsModule} from './about-us/about-us.module';
import {BeaconsModule} from './beacons/beacons.module';
import {RoomsModule} from './rooms/rooms.module';
import {MapModule} from './map/map.module';
import {ApiEntryPointModule} from './api-entry-point/api-entry-point.module';
/**
 * Import of application's components
 */
import {PagesComponent} from './pages.component';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        HttpClientModule,
        PagesRoutingModule,
        AboutUsModule,
        BeaconsModule,
        RoomsModule,
        MapModule,
        ApiEntryPointModule
    ],
    declarations: [
        PagesComponent
    ]
})
export class PagesModule {
}
