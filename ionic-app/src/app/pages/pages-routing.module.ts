import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PagesComponent} from './pages.component';
import {RoomsComponent} from './rooms/rooms.component';
import {AboutUsComponent} from './about-us/about-us.component';
import {BeaconsComponent} from './beacons/beacons.component';
import {MapComponent} from './map/map.component';
import {ApiEntryPointComponent} from './api-entry-point/api-entry-point.component';
import {RoomGuardService} from '../shared/services/guard/room-guard.service';

const routes: Routes = [
    {
        path: '',
        component: PagesComponent,
        children: [
            {
                path: '',
                redirectTo: 'room',
                pathMatch: 'full',
            },
            {
                path: 'room',
                children: [
                    {
                        path: '',
                        component: RoomsComponent
                    },
                    {
                        path: ':roomId',
                        canActivateChild: [RoomGuardService],
                        children: [
                            {
                                path: '',
                                children: [
                                    {
                                        path: '',
                                        component: BeaconsComponent
                                    },
                                    {
                                        path: 'beacon/:beaconId',
                                        component: MapComponent
                                    }
                                ]

                            }
                        ]
                    }
                ]
            },
            {
                path: 'settings',
                component: ApiEntryPointComponent
            },
            {
                path: 'about-us',
                component: AboutUsComponent
            }
        ]
    },
    {
        path: '',
        redirectTo: 'room',
        pathMatch: 'full'
    },
    {
        path: '**',
        redirectTo: 'room',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule {
}
