import {Component, OnInit} from '@angular/core';
import {ToolbarService} from '../shared/services/toolbar/toolbar.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-pages',
    template: '<ion-header>\n' +
        '    <ion-toolbar>\n' +
        '        <ion-title>\n' +
        '            {{navigationTitle()}}\n' +
        '        </ion-title>\n' +
        '        <ion-buttons [slot]="\'start\'">\n' +
        '            <ion-back-button [defaultHref]="navigationPrevious()"></ion-back-button>\n' +
        '        </ion-buttons>\n' +
        '    </ion-toolbar>\n' +
        '</ion-header>\n' +
        '<ion-content [ngClass]="\'ion-content-pages\'">\n' +
        '<ion-tabs>\n' +
        '        <ion-tab-bar [slot]="\'bottom\'">\n' +
        '            <ion-tab-button *ngFor="let item of tabs" [tab]="item?.path">\n' +
        '                <ion-icon [name]="item?.icon"></ion-icon>\n' +
        '                <ion-label>{{item?.label}}</ion-label>\n' +
        '            </ion-tab-button>\n' +
        '        </ion-tab-bar>\n' +
        '</ion-tabs>\n' +
        '</ion-content>'
})
export class PagesComponent implements OnInit {

    /**
     * @description This array is a container of individual Tab components
     */
    private _tabs: Array<any>;

    /**
     * @description Constructor of PagesComponent component
     *
     * The constructor creates an instance of the AppComponent component and specifies the default values of
     * the input and output variables of the component.
     *
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     * @param toolbarService A reference to the toolbarService of the application
     */
    constructor(private router: Router, private toolbarService: ToolbarService) {
        this.tabs = [];
    }

    /**
     * @description: This method is part of the component life cycle: ligecycle hook.
     *
     * It is called when Angular initializes the view of a component.
     * It first displays the properties related to the data and defines the input properties of the component.
     * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
     */
    ngOnInit(): void {
        this.tabs.push(
            {
                label: 'Home',
                icon: 'md-home',
                path: 'room'
            },
            {
                label: 'Settings',
                icon: 'settings',
                path: 'settings'
            },
            {
                label: 'About Us',
                icon: 'md-help-circle',
                path: 'about-us'
            }
        );
    }

    /**
     * @description This method makes it possible to retrieve the title of the pages loaded in the ToolbarService service
     */
    navigationTitle(): string {
        return this.toolbarService.title;
    }

    /**
     * @description This method makes it possible to retrieve the previous url in the browser history in the ToolbarService service
     */
    navigationPrevious(): string {
        return this.toolbarService.previousUrl;
    }

    /**
     * @description The method allows you to retrieve the container of individual Tab components
     */
    get tabs(): Array<any> {
        return this._tabs;
    }

    /**
     * @description The method allows you to assign the container of individual Tab components
     *
     * @param value Container of individual Tab components
     */
    set tabs(value: Array<any>) {
        this._tabs = value;
    }
}
