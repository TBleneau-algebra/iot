import {Location} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {RequestService} from '../request/request.service';
import {environment} from '../../../../environments/environment';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class EquipmentService extends RequestService {

    /**
     * @description Constructor of the EquipmentService.
     *
     * @param httpClient A reference to Angular's HttpClient internal service
     * @param location A reference to the rental service that allows you to build a URL from the root
     */
    constructor(httpClient: HttpClient, location: Location) {
        super(httpClient, location, environment.apiHost);
    }
}
