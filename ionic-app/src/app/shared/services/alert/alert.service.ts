import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import Swal, {SweetAlertType} from 'sweetalert2';

@Injectable({
    providedIn: 'root'
})
export class AlertService {

    /**
     * @description Constructor of the AlertService service
     */
    constructor() {
    }

    /**
     * @description This method throw an alert with title as title, message as text body and type to specify the layout to use.
     *
     * @param message The message to display
     * @param title The title of the message
     * @param type The type of the message
     * @param custom Custom CSS class
     */
    private _alert(message: string, title: string = null, type: SweetAlertType, custom?: string): Observable<boolean> {
        const subj = new Subject<boolean>();
        const ref = Swal.fire({
            titleText: title,
            text: message,
            timer: (type === 'success' || type === 'error') ? 2000 : undefined,
            showConfirmButton: (type !== 'success' && type !== 'error'),
            customClass: custom,
            type
        });

        ref.then((val) => {
            if (val.value) {
                subj.next(val.value);
            } else {
                subj.next(null);
            }
        }).catch(subj.error);
        return subj.asObservable();
    }

    /**
     * This method ask a data to the user
     *
     * @param question The question to ask
     * @param submitText The submit button label
     * @param dismissText The cancel or dismiss button label
     * @param custom Custom CSS class
     */
    public question(question: string, submitText: string = 'Ok', dismissText: string = 'Cancel', custom: string = 'swal2-popup-cst'):
        Observable<string> {
        const subj = new Subject<string>();
        const ref = Swal.fire({
            text: question,
            input: 'text',
            confirmButtonText: submitText,
            showCancelButton: true,
            cancelButtonText: dismissText,
            customClass: custom,
        });

        ref.then((val) => {
            if (val.value) {
                subj.next(val.value);
            } else {
                subj.next(null);
            }
        }).catch(subj.error);

        return subj.asObservable();
    }

    /**
     * @description This method is a shortcut for _alert(xx, xx, 'error')
     *
     * @param message The message to display
     * @param title The title of the message
     */
    public error(message: string, title: string = null): Observable<boolean> {
        return this._alert(message, title, 'error', 'swal2-popup-cst');
    }

    /**
     * @description This method is a shortcut for _alert(xx, xx, 'warning')
     *
     * @param message The message to display
     * @param title The title of the message
     */
    public warning(message: string, title: string = null): Observable<boolean> {
        return this._alert(message, title, 'warning');
    }

    /**
     * @description This method is a shortcut for _alert(xx, xx, 'info')
     *
     * @param message The message to display
     * @param title The title of the message
     */
    public info(message: string, title: string = null): Observable<boolean> {
        return this._alert(message, title, 'info');
    }

    /**
     * @description This method is a shortcut for _alert(xx, xx, 'success')
     *
     * @param message The message to display
     * @param title The title of the message
     */
    public success(message: string, title: string = null): Observable<boolean> {
        return this._alert(message, title, 'success', 'swal2-popup-cst');
    }
}
