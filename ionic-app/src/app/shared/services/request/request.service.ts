import {Location} from '@angular/common';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {RequestParamsInterface} from './params/request-params.interface';
import {RequestResponseClass} from './response/request-response.class';
import {RequestResponseInterface} from './response/request-response.interface';
import {combineLatest, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

export class RequestService {

    /**
     * @description An RxJS Subject is a special type of Observable that allows values to be multicasted to many Observers
     *
     * In this case, we observe the status of the Http query
     */
    protected unsubscribe: Subject<void> = new Subject<void>();

    /**
     * @description Value of the API context path
     */
    private _apiHost: string;

    /**
     * @description Value of the url to be requested
     */
    private _urlToRequest: string;

    /**
     * @description Manufacturer of the EquipmentService.
     *
     * @param httpClient A reference to Angular's HttpClient internal service
     * @param location A reference to the rental service that allows you to build a URL from the root
     * @param target The location of the API to use. Relative or absolute URL.
     */
    constructor(private httpClient: HttpClient, private location: Location, private target: string) {
        this.apiHost = target;
    }

    /**
     * @description This method is used to add several request parameters to the url of an Http request
     *
     * @param values All parameters to be provided to the Http request
     */
    private createQueryParams(values: Array<{ name: string, value: string }>): { [key: string]: string } {
        const queries: { [key: string]: string } = {};

        values.forEach((value) => {
            queries[value.name] = value.value;
        });
        return queries;
    }

    /**
     * @description This method is used to provide several headers to an Http request
     *
     * @param values All headers filled in in table form
     */
    private createHeaders(values: Array<{ name: string, value: string }>): { [key: string]: string[] } {
        const headers: { [key: string]: string[] } = {};

        values.forEach((header) => {
            if (!headers[header.name]) {
                headers[header.name] = [];
            }
            headers[header.name].push(header.value);
        });
        return headers;
    }

    /**
     * @description This method executes an Http request and returns a Promise.
     *
     * @param method Method used for HTTP request ('POST','PUT','GET','PATCH','DELETE')
     * @param data Data to be sent to the API when requesting HTTP
     * @param params parameters (optional) to be attached to the HTTP request if necessary
     */
    protected execute(method: string, data?: any, params?: RequestParamsInterface): Promise<RequestResponseInterface> {
        const parent: any = this;
        const id = setTimeout(function() {
            parent.cancel();
        }, 7000);

        return new Promise<RequestResponseInterface>((resolve, reject) => {
            const response: RequestResponseInterface = new RequestResponseClass();

            this.httpClient
                .request<any>(method, this.apiHost + this.urlToRequest, {
                    headers: (params !== undefined && params !== null) ? this.createHeaders(params.headers) : {},
                    params: (params !== undefined && params !== null) ? this.createQueryParams(params.params) : {},
                    body: (data !== undefined && data !== null) ? data : null,
                    observe: (params !== undefined && params !== null) ? params.observe : 'response',
                    withCredentials: (params !== undefined && params !== null) ? params.withCredentials : false,
                    responseType: (params !== undefined && params !== null) ? params.responseType : 'json',
                    reportProgress: (params !== undefined && params !== null) ? params.reportProgress : false
                })
                .pipe(takeUntil(this.unsubscribe))
                .subscribe(
                    (res: HttpResponse<any>) => {
                        clearTimeout(id);
                        response.map(res);
                        resolve(response);
                    }, (error: HttpErrorResponse) => {
                        clearTimeout(id);
                        response.mapError(error);
                        reject(response);
                    }
                );
        });
    }

    /**
     * @description This method is used to request a representation of the specified resource.
     * This GET request should only be used to retrieve data.
     *
     * @param path Value of the url to be requested
     * @param data Optional Entity(ies) to send to the specified resource
     * @param params Optional parameters to be attached to the request
     */
    async get(path: string, data?: any, params?: RequestParamsInterface): Promise<RequestResponseInterface> {
        this.urlToRequest = path;
        return await this.execute('GET', data, params);
    }

    /**
     * @description This method is used to send an entity to the specified resource (POST request).
     * This usually results in a change of state or edge effects on the server.
     *
     * @param data Entity(ies) to be sent to the specified resource
     * @param path Value of the url to be requested
     * @param params Optional parameters to be attached to the request
     */
    async post(data: any, path: string, params?: RequestParamsInterface): Promise<RequestResponseInterface> {
        this.urlToRequest = path;
        return await this.execute('POST', data, params);
    }

    /**
     * @description This method replaces all current representations of the resource covered by the content of the request (PUT request).
     *
     * @param data Entity(ies) to be sent to the specified resource
     * @param path Value of the url to be requested
     * @param params Optional parameters to be attached to the request
     */
    async put(data: any, path: string, params?: RequestParamsInterface): Promise<RequestResponseInterface> {
        this.urlToRequest = path;
        return await this.execute('PUT', data, params);
    }

    /**
     * @description This method replaces all current representations of the resource covered by
     * the content of the request (PATCH request).
     *
     * @param data Entity(ies) to be sent to the specified resource
     * @param path Value of the url to be requested
     * @param params Optional parameters to be attached to the request
     */
    async patch(data: any, path: string, params?: RequestParamsInterface): Promise<RequestResponseInterface> {
        this.urlToRequest = path;
        return await this.execute('PATCH', data, params);
    }

    /**
     * @description This DELETE method deletes the specified resource.
     *
     * @param data Entity to send to the specified resource
     * @param path Value of the url to be requested
     * @param params Optional parameters to be attached to the request
     */
    async delete(data: any, path: string, params?: RequestParamsInterface): Promise<RequestResponseInterface> {
        this.urlToRequest = path;
        return await this.execute('DELETE', data, params);
    }

    /**
     * @description The method allows you to cancel a pending request that takes too long to execute.
     */
    cancel(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    /**
     * @description This method returns the value of the API context path
     */
    get apiHost(): string {
        return this._apiHost;
    }

    /**
     * @description This method assigns the value of the context path to perform a request on the API
     *
     * @param value The value of the API context path
     */
    set apiHost(value: string) {
        this._apiHost = value;
    }

    /**
     * @description This method returns the value of the source url to be requested to request the data from the API
     */
    get urlToRequest(): string {
        return this._urlToRequest;
    }

    /**
     * @description This method assigns the value of the source url to be requested to request the data from the API
     *
     * @param value The value of the source url to request to request data from the API
     */
    set urlToRequest(value: string) {
        this._urlToRequest = value;
    }
}
