import {ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {EquipmentService} from '../equipment/equipment.service';

@Injectable({
    providedIn: 'root'
})
export class RoomGuardService implements CanActivateChild {

    /**
     * @description Construct a RoomGuardService
     *
     * @param equipmentService A reference to the EquipmentService service
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     */
    constructor(private equipmentService: EquipmentService, private router: Router) {
    }

    /**
     *  @description This method is a guard and decides if a child route can be activated
     *  If all guards return true, navigation will continue.
     *  If any guard returns false, navigation will be cancelled.
     *  If any guard returns a UrlTree, current navigation will be cancelled and a new navigation will be kicked off to the UrlTree
     *  returned from the guard.
     *
     * @param childRoute contains the information about a route associated with a component loaded in an outlet at a particular moment
     * in time.
     * @param state represents the state of the router at a moment in time.
     */
    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean |
        UrlTree> | boolean | UrlTree {
        return new Promise(resolve => {

            if (childRoute.params.hasOwnProperty('roomId')) {
                this.equipmentService.get('/floor/' + childRoute.params.roomId).then(response => {
                    (response.responseStatus === 200) ? resolve(true) : resolve(this.router.parseUrl('/room'));
                }).catch(() => resolve(this.router.parseUrl('/room')));
            } else {
                resolve(this.router.parseUrl('/room'));
            }
        });
    }
}
