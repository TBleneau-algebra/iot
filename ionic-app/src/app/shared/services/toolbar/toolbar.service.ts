import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class ToolbarService {

    /**
     * @description The title of the pages loaded
     */
    private _title: string;

    /**
     * @description The previous url in the browser history
     */
    private _previousUrl: string;

    /**
     * @description Constructor of ToolbarService service
     *
     * The constructor creates an instance of the ToolbarService service and specifies the default values of
     * the input and output variables of the component.
     *
     * @param router Angular Router enables navigation from one view to the next as users perform application tasks
     */
    constructor(private router: Router) {
    }

    /**
     * @description The variable relating to the previous url in the browser history
     */
    clear() {
        this.previousUrl = undefined;
    }

    /**
     * @description The method allows you to retrieve the title of the pages loaded
     */
    get title(): string {
        return this._title;
    }

    /**
     * @description The method allows you to assign the title of the pages loaded
     *
     * @param value Title of the pages loaded
     */
    set title(value: string) {
        this._title = value;
    }

    /**
     * @description The method allows you to retrieve the previous url in the browser history
     */
    get previousUrl(): string {
        return this._previousUrl;
    }

    /**
     * @description The method allows you to assign the previous url in the browser history
     *
     * @param value Previous url in the browser history
     */
    set previousUrl(value: string) {
        this._previousUrl = value;
    }
}
