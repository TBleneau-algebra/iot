export const environment = {
    production: false,
    development: false,

    apiHost: 'http://localhost:8080/api/v1'
};
