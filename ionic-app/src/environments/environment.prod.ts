export const environment = {
    production: true,
    development: false,

    apiHost: 'http://locatingequipment.northeurope.cloudapp.azure.com/api/v1'
};
